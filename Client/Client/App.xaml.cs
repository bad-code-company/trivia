﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Client
{

    /// <summary>
    /// Constans to the program
    /// </summary>
    public static class Constants
    {
        public const int FAILURE = 0;
        public const int MAX_BUFFER_LENGTH = 1024;
        public const int REQUEST_CODE_AND_DATA_LENGTH = 5;
        public const int INT_LENGTH = 4;
        public const int BYTE_LENGTH = 8;
        public const int HEXADECIMAL_255 = 255;
        public const int TWO_SECONDS = 2000;
        public const int THREE_SECONDS = 3000;
        public const string BUTTON_BACKGROUND_COLOR = "#2c8ded";
        public const int TIMEOUT_ANSWER_ID = 4;
    }

    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (!Communication.connect())
            {
                MessageBox.Show("Error! Couldn't connect to server");
                Shutdown();
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            //everytime the program is closed, if a user is connected, the application logs him out.
            Communication.logOut();
            base.OnExit(e);
        }
    }
}
