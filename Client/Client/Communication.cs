﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Client
{
    
    static class Communication
    {
        private const int port = 8820;
        private static TcpClient client;
        private static NetworkStream stream { get; set; }
        private static string connectedUserName;
        private static bool isLoggedIn = false;

        /// <summary>
        /// A function which connects to the Trivia server.
        /// </summary>
        /// <returns>True if connection succeeded, false otherwise</returns>
        public static bool connect()
        {
            client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            try
            {
                client.Connect(serverEndPoint);
            }
            catch(Exception connectionException)
            {
                return false;
            }

            Communication.stream = client.GetStream();
            return true;
        }

        /// <summary>
        /// A general function for sending request to the Trivia server.
        /// </summary>
        /// <typeparam name="ResponseType">
        /// The expected response type to return.
        /// ResponseType has to be a class derived from GeneralResponse.
        /// </typeparam>
        /// <param name="requestCode">The code of the request to the server.</param>
        /// <param name="json_data">The data to send to the server in json format.</param>
        /// <returns>
        /// The function returns the server's response as a Response object with the type given in ResponseType parameter.
        /// If an error occurs, the function returns an ErrorResponse object.
        /// </returns>
        public static GeneralResponse sendRequest<ResponseType>(int requestCode, string json_data) where ResponseType : GeneralResponse
        {
            if(!client.Connected)
            {
                return null; //if connection to the server was lost, the response is null.
            }

            byte[] code = { (byte)requestCode };

            byte[] length_bytes = new byte[4];

            // Adding the 4 bytes message length to a 1 byte array by taking it apart into 4 one-byte pieces.
            for (int i = 3*Constants.BYTE_LENGTH, count = 0; i >= 0; i -= Constants.BYTE_LENGTH, count++)
            {
                length_bytes[count] = (byte)((json_data.Length >> i) & Constants.HEXADECIMAL_255);
            }

            byte[] json_bytes = Encoding.ASCII.GetBytes(json_data);

            byte[] request = new byte[code.Length + length_bytes.Length + json_bytes.Length];
            System.Buffer.BlockCopy(code, 0, request, 0, code.Length);
            System.Buffer.BlockCopy(length_bytes, 0, request, code.Length, length_bytes.Length);
            System.Buffer.BlockCopy(json_bytes, 0, request, code.Length + length_bytes.Length, json_bytes.Length);

            request = Communication.encryptMessage(request);

            byte[] buffer;
            try
            {
                Communication.stream.Write(request, 0, request.Length);
                Communication.stream.Flush();
                byte[] tempBuffer = generalRead();
                buffer = new byte[tempBuffer.Length - 5];
                System.Buffer.BlockCopy(tempBuffer, 5, buffer, 0, buffer.Length);
            }
            catch(Exception e)
            {
                return null;
            }

            if (buffer[0] == (int)ResponseId.ERROR_ID)
            {
                return JsonConvert.DeserializeObject<ErrorResponse>(Encoding.ASCII.GetString(buffer));
            }
            string a = Encoding.ASCII.GetString(buffer);
            return JsonConvert.DeserializeObject<ResponseType>(Encoding.ASCII.GetString(buffer));
        }

        /// <summary>
        /// A function that reads data from the server.
        /// </summary>
        /// <returns>Returns the buffer of bytes read</returns>
        public static byte[] generalRead()
        {
            byte[] length = new byte[4];
            Communication.stream.Read(length, 0, length.Length);
            byte[] buffer = new byte[length[0] << 24 | length[1] << 16 | length[2] << 8 | length[3]];
            Communication.stream.Read(buffer, 0, buffer.Length);
            buffer = Communication.decryptMessage(buffer);
            return buffer;
        }

        /// <summary>
        /// A function to check id there is availiable data to read from the socket.
        /// </summary>
        /// <returns>True if there is available data in the socket, false otherwise</returns>
        public static bool isDataAvailiable()
        {
            return Communication.stream.DataAvailable;
        }

        /// <summary>
        /// A function that loggs out a user from the server.
        /// </summary>
        public static void logOut()
        {
            if (isLoggedIn)
                sendRequest<Response>((int)RequestId.LOGOUT_ID, "");
            isLoggedIn = false;
            connectedUserName = "";
        }

        /// <summary>
        /// A function that updates user data in Communication class when a certain user has logged in.
        /// </summary>
        /// <param name="username">The username of the user logged in</param>
        public static void userLoggedIn(string username)
        {
            isLoggedIn = true;
            connectedUserName = username;
        }

        /// <summary>
        /// A function that returns the username of the user logged in from the current client.
        /// </summary>
        /// <returns>A string contains the username of the user logged in</returns>
        public static string getUsername()
        {
            return connectedUserName;
        }

        public static byte[] encryptMessage(byte[] request)
        {
            byte[] encrypted = OTPCryptoAlgorithm.encrypt(request);
            byte[] messageLength = new byte[4];
            byte[] message = new byte[encrypted.Length + messageLength.Length];
            for (int i = 3 * Constants.BYTE_LENGTH, count = 0; i >= 0; i -= Constants.BYTE_LENGTH, count++)
            {
                messageLength[count] = (byte)((encrypted.Length >> i) & Constants.HEXADECIMAL_255);
            }
            System.Buffer.BlockCopy(messageLength, 0, message, 0, messageLength.Length);
            System.Buffer.BlockCopy(encrypted, 0, message, messageLength.Length, encrypted.Length);
            return message;
        }

        public static byte[] decryptMessage(byte[] response)
        {
            return OTPCryptoAlgorithm.decrypt(response);
        }
    }
}
