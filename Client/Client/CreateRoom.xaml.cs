﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace Client
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>

    
    public partial class CreateRoom : Window
    {
        /// <summary>
        /// Constructor of a Create Room window
        /// </summary>
        public CreateRoom()
        {
            InitializeComponent();
        }

        /// <summary>
        /// A function that returns to the logged user's menu screen.
        /// </summary>
        /// <param name="sender">The object calling the function</param>
        /// <param name="e">The arguments passed to the function</param>
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }

        /// <summary>
        /// The function will send a request to create a new room to the server.
        /// </summary>
        /// <param name="sender">The object calling the function</param>
        /// <param name="e">The arguments passed to the function</param>
        private void sendCreateRoomRequest(object sender, RoutedEventArgs e)
        {
            int intNumberOfPlayers = 0, intQuestionCount = 0, intAnswerTimeout = 0;
            
            try
            {
                //Trying to parse the data from the GUI into integers.
                intNumberOfPlayers = int.Parse(txtNumberOfPlayers.Text);
                intQuestionCount = int.Parse(txtNumberOfQuestions.Text);
                intAnswerTimeout = int.Parse(txtTimeForQuestion.Text);
            }
            catch(Exception parseException)
            {
                //If couldn't parse data, popup window opend and the GUI components are being resetted.
                MessageBox.Show("Input type was not allowed!");
                resetTextComponents();
                return;
            }
            
            GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.CREATE_ROOM_ID, JsonConvert.SerializeObject(new CreateRoomRequest { roomName = txtRoomName.Text, maxUsers = intNumberOfPlayers, questionCount = intQuestionCount, answerTimeout = intAnswerTimeout }));
            if(response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((Response)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! couldn't create room..");
                backButton_Click(null, null);
            }
            else
            {
                //if the server response is valid and affirmative, switching to the room.
                Room newRoom = new Room(txtRoomName.Text, true, intAnswerTimeout, intQuestionCount) { Left = this.Left, Top = this.Top };
                App.Current.MainWindow = newRoom;
                this.Close();
                newRoom.Show();
            }
        }

        /// <summary>
        /// A function that resets the text components in the GUI.
        /// </summary>
        private void resetTextComponents()
        {
            txtRoomName.Text = "";
            txtNumberOfPlayers.Text = "";
            txtNumberOfQuestions.Text = "";
            txtTimeForQuestion.Text = "";
        }
    }
}
