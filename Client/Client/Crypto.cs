﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client
{

    public static class OTPCryptoAlgorithm
    {
        /// <summary>
        /// The function will encrypt a buffer with OTP.
        /// </summary>
        /// <param name="buffer">the buffer to encrypt</param>
        /// <returns>the encrypted buffer</returns>
        public static byte[] encrypt(byte[] buffer)
        {
            byte[] crypted = new byte[2 * buffer.Length];
            Random randNum = new Random();
            for(int i=0; i<buffer.Length; i++)
            {
                crypted[i] = (byte)randNum.Next(0, 255);
                crypted[i+ buffer.Length] = (byte)((buffer[i] + crypted[i]) % 256);
            }
            return crypted;
        }

        /// <summary>
        /// The function will decrypt a buffer encrypted with OTP.
        /// </summary>
        /// <param name="buffer">the buffer to decrypt</param>
        /// <returns>the decrypted buffer</returns>
        public static byte[] decrypt(byte[] buffer)
        {
            byte[] crypted = new byte[buffer.Length/2], key = new byte[buffer.Length/2];
            byte[] decrypted = new byte[buffer.Length/2];
            for(int i=0; i<buffer.Length/2; i++)
            {
                key[i] = buffer[i];
                crypted[i] = buffer[i + (buffer.Length / 2)];
            }
            for(int i=0; i<crypted.Length; i++)
            {
                decrypted[i] = (byte)(crypted[i] - key[i]);
            }
            return decrypted;
        }
    }
}
