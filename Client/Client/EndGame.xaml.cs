﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Media.Effects;
using System.Linq;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for EndGame.xaml
    /// </summary>
    public partial class EndGame : Window
    {
        private BackgroundWorker listeningToServer = new BackgroundWorker();
        private bool isListeningCancledIntentionally = false;
        private List<PlayerResults> list;

        public EndGame()
        {
            InitializeComponent();

            //Changing GUI components:
            popup.Visibility = Visibility.Visible;
            listResult.Visibility = Visibility.Hidden;

            doListeningWork();
        }

        /// <summary>
        /// back button click event.
        /// </summary>
        /// <param name="sender">the button which calls the function</param>
        /// <param name="e">the arguments passed</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.LEAVE_GAME_ID, "");
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()))
            {
                MessageBox.Show("Error from server!");
            }
            Menu menu = new Menu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }

        /// <summary>
        /// A Background Worker which keeps sending getGameResult request until it gets a valid one.
        /// when it does, it calls ReportProgress.
        /// </summary>
        /// <param name="sender">the sender of the function</param>
        /// <param name="e">the arguments passed</param>
        private void doListeningWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.listeningToServer.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                GeneralResponse response = Communication.sendRequest<GetGameResultsResponse>((int)RequestId.GET_GAME_RESULT, "");
                if (response == null)
                {
                    //if server connection was lost
                    MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                    this.Close();
                }
                else if (response.GetType().IsInstanceOfType(new ErrorResponse()))
                {
                    MessageBox.Show("Error from server!");
                    btnBack_Click(null, null);
                }
                else if(((GetGameResultsResponse)response).status != Constants.FAILURE)
                {
                    list = ((GetGameResultsResponse)response).results.ToList();
                    isListeningCancledIntentionally = true;
                    listeningToServer.ReportProgress(0);
                    listeningToServer.CancelAsync();
                }
                Thread.Sleep(Constants.THREE_SECONDS); //delaying for 3 seconds
            }
        }

        /// <summary>
        /// This funciton is called when ReportProgress of the background worker is called
        /// it changes the gui and presents the game results.
        /// </summary>
        /// <param name="sender">the sender of the function</param>
        /// <param name="e">the arguments passed</param>
        private void resultsGathered(object sender, ProgressChangedEventArgs e)
        {
            list = list.OrderBy(o => o.correctAnswerCount).ToList();

            List<string> results = new List<string>();
            for (int i = 0; i < list.Count; i++)
            {
                string s = list[i].username + " ( Score: " + list[i].correctAnswerCount.ToString() + ", Avg time: " + list[i].averageAnswerTime.ToString() + " )";
                results.Insert(i, s);
            }
            listResult.ItemsSource = results;
            popup.Visibility = Visibility.Hidden;
            listResult.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// The function is called when CancledAsync is called from the backgroundWorker.
        /// if the cancellation wasn't intentional, the function presents an error.
        /// </summary>
        /// <param name="sender">the sender of the function</param>
        /// <param name="e">the arguments passed</param>
        private void listeningCancled(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (!this.isListeningCancledIntentionally)
                {
                    MessageBox.Show("Error while listening to server.  You will be left the room now!");
                    btnBack_Click(null, null);
                }
            }
        }

        /// <summary>
        /// The function resets the background worker and runs it.
        /// </summary>
        private void doListeningWork()
        {
            listeningToServer.WorkerSupportsCancellation = true;
            listeningToServer.WorkerReportsProgress = true;

            listeningToServer.DoWork += doListeningWork;
            listeningToServer.ProgressChanged += resultsGathered;
            listeningToServer.RunWorkerCompleted += listeningCancled;

            listeningToServer.RunWorkerAsync();
        }

    }
}
