﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for HighScore.xaml
    /// </summary>
    public partial class HighScore : Window
    {
        /// <summary>
        /// A Constructor for HighScore window.
        /// </summary>
        public HighScore()
        {
            InitializeComponent();
            //getting the 4 highest scored players from the server.
            GeneralResponse response = Communication.sendRequest<StatisticsResponse>((int)RequestId.GET_HIGH_SCORE_ID, "");
            Label[] lables = { player1, player2, player3, player4, player5 };
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((StatisticsResponse)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! couldn't get high scores..");
                throw new Exception();
            }
            else
            {
                //updating gui labels for highest scored players.
                StatisticsResponse statisticsResponse = (StatisticsResponse)response;
                for (int i=0; i<lables.Length && i < statisticsResponse.statistics.Count; i++)
                {
                    lables[i].Content = (i + 1) + ". " + statisticsResponse.statistics[i];
                }
            }
        }

        /// <summary>
        /// A function that returns to the logged user's menu screen.
        /// </summary>
        /// <param name="sender">The object calling the function</param>
        /// <param name="e">The arguments passed to the function</param>
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }
    }
}
