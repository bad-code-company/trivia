﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Client
{
    
    public partial class JoinRoom : Window
    {
        private BackgroundWorker refreshScreen = new BackgroundWorker(); //A screen refresher thread
        private Dictionary<string, uint> roomsDict;
        private bool isCancledIntentionally = false;

        public JoinRoom()
        {
            InitializeComponent();

            reloadAvailableRooms(this, null); //the function is called once in order to check if the server is still connected.

            doRefreshWork(); //running the thread which refreshes the users


        }

        /// <summary>
        /// Switching back to menuScreen.
        /// </summary>
        /// <param name="sender">The sender of the back command</param>
        /// <param name="e">The arguments</param>
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            isCancledIntentionally = true;
            refreshScreen.CancelAsync();
            Menu menu = new Menu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }

        /// <summary>
        /// The function will invoke a screen refresh command everty 3 seconds.
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The arguments</param>
        private void doScreenRefreshingWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.refreshScreen.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                refreshScreen.ReportProgress(0);
                Thread.Sleep(3000);
            }
        }

        /// <summary>
        /// The function sends GetRooms request to the server and updates the GUI interface
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The arguments</param>
        private void reloadAvailableRooms(object sender, ProgressChangedEventArgs e)
        {
            GeneralResponse response = Communication.sendRequest<GetRoomsResponse>((int)RequestId.GET_ROOMS_ID, "");
            if (response == null)
            {
                //if server connection was lost
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((GetRoomsResponse)response).status == Constants.FAILURE)
            {
                //if error from server is recieved
                MessageBox.Show("Error from server!");
                throw new Exception();
            }
            else
            {
                //if status is 1, no error was returned and the rooms list is updated
                this.roomsDict = new Dictionary<string, uint>();
                IList<ListBoxItem> rooms_string = new List<ListBoxItem>();
                GetRoomsResponse roomResponse = (GetRoomsResponse)response;
                for (int i = 0; i < roomResponse.rooms.Count; i++)
                {
                    ListBoxItem item = new ListBoxItem();
                    item.Content = roomResponse.rooms[i].name;
                    item.Name = "R" + roomResponse.rooms[i].id.ToString();
                    item.Tag = roomResponse.rooms[i].timePerQuestion.ToString() + "," + roomResponse.rooms[i].numOfQuestionsInGame.ToString();
                    rooms_string.Insert(i, item);
                    roomsDict[roomResponse.rooms[i].name] = roomResponse.rooms[i].id;
                }
                roomsList.ItemsSource = rooms_string;

            }
        }


        /// <summary>
        /// This function is called when refresh screen is canceled
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The arguments</param>
        void refreshCancled(object sender, RunWorkerCompletedEventArgs e){
            if (!this.isCancledIntentionally)
            {
                //if the refresh was'nt intentionally stopped, an error occured.
                MessageBox.Show("Error while loading available rooms");
                backButton_Click(null, null);
            }
        }

        /// <summary>
        /// A function which sends joinRoom request to the server and joins the selected room from the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnJoinRoom_Click(object sender, RoutedEventArgs e)
        {
            if (roomsList.SelectedItem == null)
            {
                //if no room was chosen
                MessageBox.Show("You didn't choose any room to join to...");
                return;
            }

            //converting selected item into ListBoxItem
            object selectedItem = roomsList.SelectedItem;
            ListBoxItem selectedListBoxItem = roomsList.ItemContainerGenerator.ContainerFromItem(selectedItem) as ListBoxItem;
            
            GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.JOIN_ROOM_ID, JsonConvert.SerializeObject(new JoinRoomRequest { roomId = uint.Parse(selectedListBoxItem.Name[1].ToString()) }));
            if (response == null)
            {
                //if server connection was lost
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((Response)response).status == Constants.FAILURE)
            {
                //if an error is recieved
                MessageBox.Show("Error from server! couldn't join to the selected room..");
            }
            else
            {
                this.isCancledIntentionally = true;

                //using the tag attribute to pass room data:
                string[] data = selectedListBoxItem.Tag.ToString().Split(',');

                int timeForQuestion = int.Parse(data[0]);
                int questionsCount = int.Parse(data[1]);

                Room room = new Room(selectedListBoxItem.Content.ToString(), false, timeForQuestion, questionsCount) { Left = this.Left, Top = this.Top };
                refreshScreen.CancelAsync(); //canceling refresh screen
                
                //changing screen:
                App.Current.MainWindow = room;
                this.Close();
                room.Show();
            }
        }

        /// <summary>
        /// The function resets the background worker and runs it.
        /// </summary>
        private void doRefreshWork()
        {
            refreshScreen.WorkerSupportsCancellation = true;
            refreshScreen.WorkerReportsProgress = true;

            //setting Thread's attributes:
            refreshScreen.DoWork += doScreenRefreshingWork;
            refreshScreen.ProgressChanged += reloadAvailableRooms;
            refreshScreen.RunWorkerCompleted += refreshCancled;

            refreshScreen.RunWorkerAsync(); //run
        }
    }
}
