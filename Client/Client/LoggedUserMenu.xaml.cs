﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    /// 

    public partial class Menu : Window
    {

        public Menu()
        {
            InitializeComponent();
            headline.Content = "Hello, " + Communication.getUsername();
        }
        /// <summary>
        /// Switching to statistics screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void statsButton_Click(object sender, RoutedEventArgs e)
        {
            Statistics stats;
            try
            {
                stats = new Statistics() { Left = this.Left, Top = this.Top };
            }
            catch(Exception statisticsException)
            {
                return;
            }
            App.Current.MainWindow = stats;
            this.Close();
            stats.Show();
        }

        /// <summary>
        /// Switching to create room screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateRoom create = new CreateRoom() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = create;
            this.Close();
            create.Show();
        }

        /// <summary>
        /// Logging out and switching to main menu screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logOutButton_Click(object sender, RoutedEventArgs e)
        {
            Communication.logOut();
            MainMenu menu = new MainMenu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }

        /// <summary>
        /// Switching to join room screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void joinRoom_Click(object sender, RoutedEventArgs e)
        {
            JoinRoom join;
            try
            {
                join = new JoinRoom() { Left = this.Left, Top = this.Top };
            }
            catch(Exception joinRoomException)
            {
                return;
            }
            App.Current.MainWindow = join;
            this.Close();
            join.Show();
        }

        /// <summary>
        /// Switching to high score screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHighScore_Click(object sender, RoutedEventArgs e)
        {
            HighScore highScore;
            try
            {
                highScore = new HighScore() { Left = this.Left, Top = this.Top };
            }
            catch (Exception joinRoomException)
            {
                return;
            }
            App.Current.MainWindow = highScore;
            this.Close();
            highScore.Show();
        }
    }
}
