﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace Client
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    /// 

    public partial class Login : Window
    {        
        public Login()
        {
            InitializeComponent();
        }
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu menu = new MainMenu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }

        private void sendLoginRequest(object sender, RoutedEventArgs e)
        {
            GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.LOGIN_ID, JsonConvert.SerializeObject(new LoginRequest { username = txtUsername.Text, password = txtPassword.Password }));
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((Response)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! couldn't log in..");
                backButton_Click(null, null);
            }
            else
            {
                Communication.userLoggedIn(txtUsername.Text);
                Menu loggedMenu = new Menu() { Left = this.Left, Top = this.Top };
                App.Current.MainWindow = loggedMenu;
                this.Close();
                loggedMenu.Show();
            }
        }
    }
}
