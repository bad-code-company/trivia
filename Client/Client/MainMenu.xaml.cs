﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// closing the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// switch to login screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = login;
            this.Close();
            login.Show();
        }

        /// <summary>
        /// switch to signup screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signupButton_Click(object sender, RoutedEventArgs e)
        {
            Signup signup = new Signup() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = signup;
            this.Close();
            signup.Show();
        }
    }
}
