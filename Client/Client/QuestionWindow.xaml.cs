﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;


namespace Client
{
    /// <summary>
    /// Interaction logic for QuestionWindow.xaml
    /// </summary>
    public partial class QuestionWindow : Window
    {
        private DispatcherTimer timer = new DispatcherTimer();
        private int timeForQuestion;
        private int questionsNumber;
        private int timeLeft;
        private int count;

        public QuestionWindow(int questionTime, int questionsNum)
        {
            InitializeComponent();
            this.timeForQuestion = questionTime;
            this.questionsNumber = questionsNum;
            count = 0;
            updateGUI();

            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
        }

        /// <summary>
        /// changing the timer label every second
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timer_Tick(object sender, EventArgs e)
        {
            if (timeLeft == 0)
            {
                answer_Click(new Button { Tag = Constants.TIMEOUT_ANSWER_ID }, null);
                //if the time has ended, sending timeout answer to the server
            }
            lblTime.Content = timeLeft;
            timeLeft--;
        }

        /// <summary>
        /// if an answer was clicked, this function is being called
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void answer_Click(object sender, RoutedEventArgs e)
        {
            uint code = uint.Parse( ((Button)sender).Tag.ToString() ); //using tag to pass answer data

            GeneralResponse response = Communication.sendRequest<SubmitAnswerResponse>((int)RequestId.SUBMIT_ANSWER_ID, JsonConvert.SerializeObject(new SubmitAnswerRequest { answerId = code }));
            if (response == null)
            {
                //if connection was lost
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((SubmitAnswerResponse)response).status == Constants.FAILURE)
            {
                // if an error occured
                MessageBox.Show("Error from server! couldn't log in..");
                Menu loggedMenu = new Menu() { Left = this.Left, Top = this.Top };
                App.Current.MainWindow = loggedMenu;
                this.Close();
                loggedMenu.Show();
            }
            else
            {
                //submitting answer:

                SubmitAnswerResponse submitAnswerResponse = (SubmitAnswerResponse)response;
                int id = submitAnswerResponse.correctAnswerId;
                Button[] answers = { btnAns0, btnAns1, btnAns2, btnAns3 };
                for(int i=0; i<answers.Length; i++)
                {
                    if(i == id)
                    {
                        answers[i].Style = (Style)FindResource("rightAnswer");
                    }
                    else
                    {
                        answers[i].Style = (Style)FindResource("wrongAnswer");
                    }
                    answers[i].IsEnabled = false;
                }
                timer.Stop();
                await Task.Delay(Constants.TWO_SECONDS);
            }
            updateGUI(); //updating GUI components to the next question
        }

        /// <summary>
        /// A function which updates the GUI components to the next question
        /// </summary>
        private void updateGUI()
        {
            GeneralResponse response = Communication.sendRequest<GetQuestionResponse>((int)RequestId.GET_QUESTION_ID, "");
            if (response == null)
            {
                //if server connection was lost
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
                return;
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()))
            {
                //if an error occured
                MessageBox.Show("Error From Server");
                this.Close();
                return;
            }
            else if (((GetQuestionResponse)response).status == Constants.FAILURE)
            {
                //if the game has ended
                EndGame end = new EndGame() { Left = this.Left, Top = this.Top };
                App.Current.MainWindow = end;
                this.Close();
                end.Show();
                return;
            }

            count++; 

            questionNumber.Content = "Question " + count + "/" + this.questionsNumber; 

            //sending next question request and updating GUI:
            GetQuestionResponse questionResponse = (GetQuestionResponse)response;
            question.Text = questionResponse.question;
            Button[] buttons = { btnAns0, btnAns1, btnAns2, btnAns3 };
            for(int i=0; i<buttons.Length; i++)
            {
                buttons[i].Content = questionResponse.answers[i][i.ToString()];
                buttons[i].Style = (Style)FindResource("RoundCorner");
                buttons[i].IsEnabled = true;
            }

            timeLeft = timeForQuestion; //resetting the time left
            timer.Start(); //starting the timer
        }
    }
}
