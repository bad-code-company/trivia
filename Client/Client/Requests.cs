﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client
{
    enum RequestId
    {
        LOGIN_ID = 10,
        SIGNUP_ID = 20,
        LOGOUT_ID = 30,
        GET_ROOMS_ID = 40,
        GET_PLAYERS_IN_ROOM_ID = 50,
        GET_HIGH_SCORE_ID = 60,
        GET_PERSONAL_STATS_ID = 70,
        JOIN_ROOM_ID = 80,
        CREATE_ROOM_ID = 90,
        CLOSE_ROOM_ID = 100,
        START_GAME_ID = 110,
        GET_ROOM_STATE_ID = 120,
        LEAVE_ROOM_ID = 130,
        LEAVE_GAME_ID = 140,
        GET_QUESTION_ID = 150,
        SUBMIT_ANSWER_ID = 160,
        GET_GAME_RESULT = 170
    };

    public class LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class PlayersInRoomRequest
    {
        public int id { get; set; }
    }

    public class CreateRoomRequest
    {
        public string roomName { get; set; }
        public int maxUsers { get; set; }
        public int questionCount { get; set; }
        public int answerTimeout { get; set; }
    }

    public class SignupRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }

    public class JoinRoomRequest
    {
        public uint roomId { get; set; }
    }

    public class SubmitAnswerRequest
    {
        public uint answerId { get; set; }
    }
}
