﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client
{
    enum ResponseId
    {
        ERROR_ID = 1,
        LOGIN_ID = 11,
        SIGNUP_ID = 21,
        LOGOUT_ID = 31,
        GET_ROOMS_ID = 41,
        GET_PLAYERS_IN_ROOM_ID = 51,
        GET_HIGH_SCORE_ID = 61,
        GET_PERSONAL_STATS_ID = 71,
        JOIN_ROOM_ID = 81,
        CREATE_ROOM_ID = 91,
        CLOSE_ROOM_ID = 101,
        START_GAME_ID = 111,
        GET_ROOM_STATE_ID = 121,
        LEAVE_ROOM_ID = 131,
        LEAVE_GAME_ID = 141,
        GET_QUESTION_ID = 151,
        SUBMIT_ANSWER_ID = 161,
        GET_GAME_RESULT_ID = 171
    };

    public class GeneralResponse{}

    public class ErrorResponse : GeneralResponse
    {
        public string message { get; set; }
    }

    public class Response : GeneralResponse
    {
        public int status { get; set; }
    }

    public class PlayersInRoomResponse : GeneralResponse
    {
        public IList<string> players;
    }

    public class StatisticsResponse : GeneralResponse
    {
        public int status { get; set; }
        public IList<string> statistics { get; set; }
    }

    public class GetRoomsResponse : GeneralResponse
    {
        public int status { get; set; }
        public IList<RoomData> rooms { get; set; }
    }

    public class GetRoomStateResponse : GeneralResponse
    {
        public int status { get; set; }
        public bool hasGameBegun { get; set; }
        public IList<string> players { get; set; }
        public int questionCount { get; set; }
        public int answerTimeout { get; set; }
    }

    public class RoomData
    {
        public uint id { get; set; }
        public string name { get; set; }
        public int maxPlayers { get; set; }
        public int numOfQuestionsInGame { get; set; }
        public int timePerQuestion { get; set; }
        public int isActive { get; set; }
    }

    public class GetQuestionResponse : GeneralResponse
    {
        public uint status { get; set; }
        public string question { get; set; }
        public IList<IDictionary<string, string>> answers { get; set; }
    }

    public class SubmitAnswerResponse : GeneralResponse
    {
        public int status { get; set; }
        public int correctAnswerId { get; set; }
    }

    public class StartGameResponse : GeneralResponse
    {
        public int status { get; set; }
    }

    public class PlayerResults
    {
        public string username { get; set; }
        public uint correctAnswerCount { get; set; }
        public uint wrongAnswerCount { get; set; }
        public float averageAnswerTime { get; set; }
    }

    public class GetGameResultsResponse : GeneralResponse
    {
        public uint status { get; set; }
        public IList<PlayerResults> results { get; set; }
    }
}
