﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Media.Effects;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for Room.xaml
    /// </summary>


    public partial class Room : Window
    {
        private bool isAdmin;
        private int timeForQuestion;
        private int questionsCount;
        private BackgroundWorker refreshScreen = new BackgroundWorker();
        private BackgroundWorker listeningToServer = new BackgroundWorker();
        private bool isRefreshCancledIntentionally = false;
        private bool isListeningCancledIntentionally = false;
        private Mutex lockStream;

        public Room(string roomName, bool isAdmin, int timeForQuestion, int questionsCount)
        {
            InitializeComponent();
            this.timeForQuestion = timeForQuestion;
            this.questionsCount = questionsCount;
            popup.Visibility = Visibility.Hidden;
            this.isAdmin = isAdmin;
            txtRoomName.Content = "Room: " + roomName;
            if (!isAdmin)
            {
                btnCloseRoom.Content = "Leave Game";
                btnStartGame.IsEnabled = false;
            }
            lockStream = new Mutex();
            startScreenRefresh();
            doListeningWork();
        }

        /// <summary>
        /// The function will close the current room
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void closeRoom_Click(object sender, RoutedEventArgs e)
        {
            //canceling refresh and server listening:
            this.isRefreshCancledIntentionally = true;
            this.isListeningCancledIntentionally = true;
            refreshScreen.CancelAsync();
            listeningToServer.CancelAsync();

            if (isAdmin)
            {
                lockStream.WaitOne();
                GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.CLOSE_ROOM_ID, "");
                lockStream.ReleaseMutex();
                if (!response.GetType().IsInstanceOfType(new ErrorResponse()) && ((Response)response).status != Constants.FAILURE)
                {
                    //room was closed!
                }
            }
            else
            {
                if (sender != null)
                {
                    //leaving game
                    lockStream.WaitOne();
                    GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.LEAVE_ROOM_ID, "");
                    lockStream.ReleaseMutex();
                    if (response == null)
                    {
                        MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                        this.Close();
                    }
                    else if (!response.GetType().IsInstanceOfType(new ErrorResponse()) && ((Response)response).status != Constants.FAILURE)
                    {
                        //left game!
                    }
                }
                else
                {
                    //showing popup message to participants when the admin closed the room
                    btnCloseRoom.IsEnabled = false;
                    BlurEffect blur = new BlurEffect();
                    blur.KernelType = KernelType.Gaussian;
                    blur.Radius = 5;
                    blur.RenderingBias = RenderingBias.Performance;
                    border.Effect = blur;
                    popup.Visibility = Visibility.Visible;
                    await Task.Delay(Constants.TWO_SECONDS);
                }
            }

            //quitting to menu
            Menu menu = new Menu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }


        /// <summary>
        /// Starts the game.
        /// </summary>
        /// <param name="sender">The sender of the question. if sender is null, the current user isn't the admin</param>
        /// <param name="e"></param>
        private void btnStartGame_Click(object sender, RoutedEventArgs e)
        {
            if(sender == null && e == null)
            {
                //canceling refresh and listening work:
                this.isRefreshCancledIntentionally = true;
                this.isListeningCancledIntentionally = true;
                refreshScreen.CancelAsync();
                listeningToServer.CancelAsync();

                lockStream.WaitOne();
                QuestionWindow game = new QuestionWindow(this.timeForQuestion, this.questionsCount) { Left = this.Left, Top = this.Top };
                lockStream.ReleaseMutex();

                //starting the game:
                App.Current.MainWindow = game;
                this.Close();
                game.Show();
                return;
            }

            lockStream.WaitOne();
            GeneralResponse response = Communication.sendRequest<StartGameResponse>((int)RequestId.START_GAME_ID, "");
            lockStream.ReleaseMutex();
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((StartGameResponse)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! Room will be closed!");
                closeRoom_Click(null, null);
            }
            else
            {
                this.isRefreshCancledIntentionally = true;
                this.isListeningCancledIntentionally = true;
                refreshScreen.CancelAsync();
                listeningToServer.CancelAsync();

                lockStream.WaitOne();
                QuestionWindow game = new QuestionWindow(this.timeForQuestion, this.questionsCount) { Left = this.Left, Top = this.Top };
                lockStream.ReleaseMutex();

                App.Current.MainWindow = game;
                this.Close();
                game.Show();
            }
        }

        /// <summary>
        /// The function refreshes the screen everty 3 seconds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void doScreenRefreshWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.refreshScreen.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                refreshScreen.ReportProgress(0);
                Thread.Sleep(Constants.THREE_SECONDS);
            }
        }

        /// <summary>
        /// The function updates the users list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reloadConnectedUsers(object sender, ProgressChangedEventArgs e)
        {
            lockStream.WaitOne();
            GeneralResponse response = Communication.sendRequest<GetRoomStateResponse>((int)RequestId.GET_ROOM_STATE_ID, "");
            lockStream.ReleaseMutex();
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((GetRoomStateResponse)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! Room will be closed!");
                closeRoom_Click(null, null);
            }
            else
            {
                //updating users list
                IList<string> rooms_string = new List<string>();
                GetRoomStateResponse roomResponse = (GetRoomStateResponse)response;
                for (int i = 0; i < roomResponse.players.Count; i++)
                {
                    rooms_string.Insert(i, roomResponse.players[i]);
                }
                usersList.ItemsSource = rooms_string;
            }
        }

        /// <summary>
        /// Called when screen refresh was canceled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshCancled(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!this.isRefreshCancledIntentionally)
            {
                MessageBox.Show("Error while loading connected players. Room will be closed!");
                closeRoom_Click(null, null);
            }
        }

        /// <summary>
        /// The function will always listen to the server to check if game admin has started it or closed the room
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void doListeningWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.refreshScreen.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                this.lockStream.WaitOne(); //locking the stream mutex
                if (Communication.isDataAvailiable())
                {
                    byte[] buffer = Communication.generalRead();
                    if (buffer[0] == (int)ResponseId.LEAVE_ROOM_ID)
                    {
                        listeningToServer.ReportProgress((int)ResponseId.LEAVE_ROOM_ID); //admin has closed the room
                    }
                    else if(buffer[0] == (int)ResponseId.START_GAME_ID)
                    {
                        listeningToServer.ReportProgress((int)ResponseId.START_GAME_ID); //admin has started the game
                    }

                }
                this.lockStream.ReleaseMutex();
            }
        }

        /// <summary>
        /// The function is called when non-admin user is being notified on change in the room state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void roomStateChanged(object sender, ProgressChangedEventArgs e)
        {
            switch(e.ProgressPercentage){
                case (int)ResponseId.LEAVE_ROOM_ID: //if the room was closed by admin
                    closeRoom_Click(null, null);
                    break;
                case (int)ResponseId.START_GAME_ID: ///if the game was started
                    btnStartGame_Click(null, null);
                    break;
            }
        }

        /// <summary>
        /// Called when listening work was canceled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listeningCancled(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!this.isListeningCancledIntentionally)
            {
                if (isAdmin)
                {
                    MessageBox.Show("Error while listening to server. Room will be closed!");
                }
                else
                {
                    MessageBox.Show("Error while listening to server.  You will be left the room now!");
                }
                closeRoom_Click(null, null);
            }
        }

        /// <summary>
        /// Starting screen refreshing
        /// </summary>
        private void startScreenRefresh()
        {
            refreshScreen.WorkerSupportsCancellation = true;
            refreshScreen.WorkerReportsProgress = true;

            refreshScreen.DoWork += doScreenRefreshWork;
            refreshScreen.ProgressChanged += reloadConnectedUsers;
            refreshScreen.RunWorkerCompleted += refreshCancled;

            refreshScreen.RunWorkerAsync();
        }

        /// <summary>
        /// Starting listening to server
        /// </summary>
        private void doListeningWork()
        {
            listeningToServer.WorkerSupportsCancellation = true;
            listeningToServer.WorkerReportsProgress = true;

            listeningToServer.DoWork += doListeningWork;
            listeningToServer.ProgressChanged += roomStateChanged;
            listeningToServer.RunWorkerCompleted += listeningCancled;

            listeningToServer.RunWorkerAsync();
        }

    }
}
