﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    /// 
    
    public partial class Signup : Window
    {
        public Signup()
        {
            InitializeComponent();
        }

        /// <summary>
        /// returns to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu menu = new MainMenu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }

        /// <summary>
        /// signing up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendSignupnRequest(object sender, RoutedEventArgs e)
        {
            GeneralResponse response = Communication.sendRequest<Response>((int)RequestId.SIGNUP_ID, JsonConvert.SerializeObject(new SignupRequest { username = txtUsername.Text, password = txtPassword.Password, email = txtEmail.Text }));
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((Response)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! couldn't sign up..");
                backButton_Click(null, null);
            }
            else
            {
                MessageBox.Show("signed up successfuly!");
            }
            backButton_Click(null, null);
        }
    }
}
