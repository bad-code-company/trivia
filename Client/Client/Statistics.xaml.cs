﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    
    public partial class Statistics : Window
    {
        public Statistics()
        {
            InitializeComponent();
            GeneralResponse response = Communication.sendRequest<StatisticsResponse>((int)RequestId.GET_PERSONAL_STATS_ID, "");
            if (response == null)
            {
                MessageBox.Show("Server connection was lost..\nThe application will be closed!");
                this.Close();
            }
            else if (response.GetType().IsInstanceOfType(new ErrorResponse()) || ((StatisticsResponse)response).status == Constants.FAILURE)
            {
                MessageBox.Show("Error from server! couldn't get statistics..");
                throw new Exception();
            }
            else
            {
                //updating statistics
                gamesPlayed.Content = ((StatisticsResponse)response).statistics[0];
                totalAnswers.Content = ((StatisticsResponse)response).statistics[1];
                correctAnswers.Content = ((StatisticsResponse)response).statistics[2];
                avgAnsTime.Content = ((StatisticsResponse)response).statistics[3];
            }
        }

        /// <summary>
        /// back to main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu() { Left = this.Left, Top = this.Top };
            App.Current.MainWindow = menu;
            this.Close();
            menu.Show();
        }
    }
}
