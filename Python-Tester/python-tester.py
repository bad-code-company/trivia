import socket
import json
 
HOST = '127.0.0.1' # localhost
LOGIN = 1
SIGNUP = 2
EXIT = 3

def main():
    port = 0
 
    while port < 1024 or port > 65535: # port number has to be a value between 1024  and 65535
        try:
            port = int( input( "Enter port to connect to (1024-65535): " ) )
        except Exception as e:
            print( "Integer type was expected! ")
 
    with socket.socket( socket.AF_INET, socket.SOCK_STREAM ) as s:
        try:
            s.connect( ( HOST, port ) ) # connecting to port
        except Exception as e:
            print( "Couldn't connect to the given port!" )
            return # exit from function
        
        while True:
            choice = 0
            username = ""
            password = ""
            email = ""
            json_dict = {}
            msg_code = 0
            
            #choosing between login and signup
            while choice != LOGIN and choice != SIGNUP and choice != EXIT:
                try:
                    choice = int( input( "1 - login\n2 - signup\n3 - exit\n" ) )
                except Exception as e:
                    print( "Integer type was expected! ")
            
            if choice == EXIT:
                break
            
            #intaking data:
            try:
                username = input( "Enter username: " )
                password = input( "Enter password: " )
            except Exception as e:
                print( "error while intaking input!")
                break
                
                
            #if login option as chosen, building json
            if choice == LOGIN:
                json_dict = {"username":username, "password":password}
                msg_code = 10
            
            #if signup option as chosen, intaking email and building json
            if choice == SIGNUP:
                try:
                    email = input( "Enter mail: " )
                except Exception as e:
                    print( "error while intaking input!")
                    break
                json_dict = {"username":username, "password":password, "mail":email}
                msg_code = 20
			
            #sending json to server
            data = ""
            length = len( data )
			
            code = bytearray()
            code.append( msg_code )

            s.sendall( code + length.to_bytes( 4, 'big' ) + data.encode() )
            
            bytes_data = s.recv( 1024 )
            data = bytes_data[5:].decode()
            
            try:
                response_json = json.loads(data)
            except Exception as e:
                print("couldn't parse data from server into json format")
                break
            
            if "message" in response_json:
                print("Response: ERROR!", response_json['message'])
            elif "status" in response_json:
                print("Response: status =", response_json['status'])
            else:
                print("Invalid json recieved from server")
            
if __name__ == "__main__":
    main()
