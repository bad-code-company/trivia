# Trivia
Trivia is a game where the competitors are asked questions about interesting but unimportant facts in many subjects
## Screenshots
![](Screenshots/main-menu-screenshot.png)
## Options
To disable MongoDB usage, you may remove **MONGO_DATABASE** define from Visual Studio project settings (Server Property -> Configuration Properties -> C/C++ -> Preprocessor -> Preprocessor Definitions).
## Dependencies
* Visual Studio with C++ 14 Compiler
### Database
Choose one database that will run on the server.
You must to install **static** libraries for the correct project work.
#### MongoDB
* MongoDB-Client (See [MongoDB Installation](#mongodb-installation))
* MongoDB C++ Driver (See [MongoDB Installation](#mongodb-installation))
##### MongoDB Installation
1. Install the MongoDB-Client from [official MongoDB web-site](https://docs.mongodb.com/manual/installation).
2. Install the MongoDB C++ Driver with [vcpkg](https://github.com/microsoft/vcpkg):
    1. Open the command line: `cmd`
    2. Open the C-drive root folder: `cd C:\`
    3. Execute a clone command: `git clone https://github.com/microsoft/vcpkg.git`
    4. Open vcpkg folder and run a bat-file or shell-file to install vcpkg:
        * `cd vcpkg`
        * `.\bootstrap-vcpkg.bat` or `.\bootstrap-vcpkg.sh`
    5. Install MongoDB C++ Driver (x86 and x64):
        * `.\vcpkg install mongo-cxx-driver:x86-windows-static`
        * `.\vcpkg install mongo-cxx-driver:x64-windows-static`
    6. Execute an integrate command: `.\vcpkg integrate install`
#### SQLite
* SQLite C Driver (See [SQLite Installation](#sqlite-installation))
##### SQLite Installation
1. Install the SQLite C Driver with [vcpkg](https://github.com/microsoft/vcpkg):
	1. Open the command line: `cmd`
    2. Open the C-drive root folder: `cd C:\`
    3. Execute a clone command: `git clone https://github.com/microsoft/vcpkg.git`
    4. Open vcpkg folder and run a bat-file or shell-file to install vcpkg:
        * `cd vcpkg`
        * `.\bootstrap-vcpkg.bat` or `.\bootstrap-vcpkg.sh`
    5. Install SQLite C Driver (x86 and x64):
        * `.\vcpkg install sqlite3:x86-windows-static`
        * `.\vcpkg install sqlite3:x64-windows-static`
    6. Execute an integrate command: `.\vcpkg integrate install`
	