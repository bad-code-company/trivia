#include "Communicator.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "GameManager.h"
#include "OTPCryptoAlgorithm.h"
#include <iostream>
#include <thread>

#define PORT 8820

Communicator &Communicator::Instance() {
	static Communicator instance;
	return instance;
}

Communicator::Communicator() : m_serverSocket( ::socket( AF_INET, SOCK_STREAM, IPPROTO_TCP ) ), m_cryptoAlgorithm( &OTPCryptoAlgorithm::Instance() ) {
	if ( this->m_serverSocket == INVALID_SOCKET ) throw std::exception( __FUNCTION__ " - socket" );
}

Communicator::~Communicator() {
	try {
		::closesocket( this->m_serverSocket );
	} catch ( ... ) {}
}

void Communicator::startHandleRequests() {
	this->bindAndListen();
	while ( true ) {
		// accept
		SOCKET clientSocket = ::accept( this->m_serverSocket, nullptr, nullptr );
		if ( clientSocket == INVALID_SOCKET ) throw std::exception( __FUNCTION__ " - socket" );
		// create thread which will communicate with client
		std::thread clientThread( &Communicator::handleNewClient, this, clientSocket );
		clientThread.detach();
	}
}

void Communicator::bindAndListen() {
	// init socket addr_in struct
	struct sockaddr_in sa;
	sa.sin_port = ::htons( PORT );
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;
	// bind
	if ( ::bind( this->m_serverSocket, reinterpret_cast<sockaddr *>( &sa ), sizeof( sa ) ) == SOCKET_ERROR )
		// throw exception if error
		throw std::exception( __FUNCTION__ " - bind" );
	// start listen
	if ( ::listen( this->m_serverSocket, SOMAXCONN ) == SOCKET_ERROR )
		// throw exception if error
		throw std::exception( __FUNCTION__ " - listen" );
}

void Communicator::handleNewClient( SOCKET clientSocket ) {
	try {
		// insert user to map
		this->m_clients.insert( std::make_pair( clientSocket, RequestHandlerFactory::Instance().createLoginRequestHandler() ) );
		while ( true ) {
			RequestInfo requestInfo = this->recvRequestInfo( clientSocket );
			// creating response buffer
			std::vector<uint8_t> responseBuffer;
			// checking that request is relevant
			if ( this->m_clients[clientSocket] && this->m_clients[clientSocket]->isRequestRelevant( requestInfo ) ) {
				// getting request result
				RequestResult requestResult = this->m_clients[clientSocket]->handleRequest( requestInfo );
				// if handler changed, changing the handler to a new one
				if ( requestResult.m_newHandler != this->m_clients[clientSocket] ) {
					delete this->m_clients[clientSocket];
					this->m_clients[clientSocket] = requestResult.m_newHandler;
				}
				responseBuffer = requestResult.m_buffer;
			} else {
				responseBuffer = JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Invalid request" } );
			}
			// sending the response
			this->sendMessage( clientSocket, responseBuffer );
		}
	} catch ( const std::exception &e ) {
		// cleaning user info from managers
		try {
			// get its username
			std::string username( this->m_clients[clientSocket]->getUsername() );
			// remove user from games
			for ( auto &&game : GameManager::Instance().getGames() ) {
				try {
					// does user into the room
					auto userList = game.getUserList();
					if ( !userList.empty() && std::find( userList.begin(), userList.end(), LoggedUser( username ) ) != userList.end() ) {
						// handler leave game
						this->m_clients[clientSocket]->handleRequest( RequestInfo{ ::time( nullptr ), RequestId::LEAVE_GAME_ID, std::vector<uint8_t>() } );
						// check that games' vector is not empty (without it, server may crash)
						if ( GameManager::Instance().getGames().empty() ) break;
					}
				} catch ( ... ) {}
			}
			// remove user from rooms
			for ( auto &&room : RoomManager::Instance().getRoomsRefs() ) {
				try {
					// does user into the room
					auto allUsers = room.second.getAllUsers();
					if ( !allUsers.empty() && std::find( allUsers.begin(), allUsers.end(), username ) != allUsers.end() ) {
						// if user is admin of the room, so delete the room, else remove user from it
						if ( dynamic_cast<RoomAdminRequestHandler *>( this->m_clients[clientSocket] ) != nullptr ) {
							// handle close room
							this->m_clients[clientSocket]->handleRequest( RequestInfo{ ::time( nullptr ), RequestId::CLOSE_ROOM_ID, std::vector<uint8_t>() } );
							// check that rooms' map is not empty (without it, server may crash)
							if ( RoomManager::Instance().getRoomsRefs().empty() ) break;
						} else {
							// handle leave room
							this->m_clients[clientSocket]->handleRequest( RequestInfo{ ::time( nullptr ), RequestId::LEAVE_ROOM_ID, std::vector<uint8_t>() } );
						}
					}
				} catch ( ... ) {}
			}
			// logout
			LoginManager::Instance().logout( username );
		} catch ( ... ) {}
		// remove request handler
		if ( this->m_clients[clientSocket] ) delete this->m_clients[clientSocket];
		// remove user from map
		this->m_clients.erase( clientSocket );
		// close socket
		::closesocket( clientSocket );
		std::cout << "Disconnected. Reason: " << e.what() << std::endl;
	}
}

RequestInfo Communicator::recvRequestInfo( SOCKET socket ) const {
	uint32_t cryptedSize = ::ntohl( Communicator::readFromSocket<uint32_t>( socket ) ); // ::ntohl for fix number bytes
	std::vector<uint8_t> cryptedBuffer = Communicator::readFromSocketWithSize( socket, cryptedSize ); // recv crypted buffer
	std::vector<uint8_t> decryptedBuffer = this->m_cryptoAlgorithm->decrypt( cryptedBuffer );
	// build a request info from decrypted buffer
	RequestInfo requestInfo{ ::time( nullptr ), static_cast<RequestId>( decryptedBuffer[0] ) }; // set recv time and requestId
	size_t size = decryptedBuffer[1] << 24 | decryptedBuffer[2] << 16 | decryptedBuffer[3] << 8 | decryptedBuffer[4]; // decrypted buffer size
	for ( size_t i = 0; i < size; ++i ) requestInfo.m_buffer.push_back( decryptedBuffer[static_cast<size_t>( 5 ) + i] ); // push buffer to struct
	return requestInfo;
}

std::pair<const SOCKET, IRequestHandler *> &Communicator::getUser( const std::string &username ) {
	for ( auto &&client : m_clients ) {
		if ( dynamic_cast<RoomMemberRequestHandler *>( client.second ) != nullptr && dynamic_cast<RoomMemberRequestHandler *>( client.second )->getUsername() == username ) {
			return client;
		}
	}
	throw std::exception( "No user with this username" );
}

void Communicator::sendMessage( const SOCKET socket, const std::vector<uint8_t> &message ) {
	// encrypt message
	std::vector<uint8_t> cryptedMessage( this->m_cryptoAlgorithm->encrypt( message ) );
	std::vector<uint8_t> buffer; // message to send
	// Adding the 4 bytes message length to a 1 byte vector by taking it apart into 4 one-byte pieces.
	for ( int i = 3 * BYTE_LENGTH; i >= 0; i -= BYTE_LENGTH ) {
		buffer.push_back( ( cryptedMessage.size() >> i ) & HEXADECIMAL_255 );
	}
	// insert a crypted message
	buffer.insert( buffer.end(), cryptedMessage.begin(), cryptedMessage.end() );
	// send a message
	if ( ::send( socket, reinterpret_cast<char *>( buffer.data() ), static_cast<int>( buffer.size() ), 0 ) == SOCKET_ERROR ) throw std::exception( "Error while sending message to client" );
}

std::vector<uint8_t> Communicator::readFromSocketWithSize( SOCKET socket, size_t size ) {
	std::vector<uint8_t> bytes( size );
	if ( size == 0 ) return bytes;
	if ( ::recv( socket, reinterpret_cast<char *>( bytes.data() ), static_cast<int>( size ), 0 ) <= 0 ) throw std::exception( "Error while recieving from socket" );
	return bytes;
}
