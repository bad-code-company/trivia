#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <winsock2.h>
#include <windows.h>
#include <cstdint>
#include <map>
#include <vector>
#include "IRequestHandler.h"
#include "ICryptoAlgorithm.h"

class Communicator {
private:
	/// fields
	SOCKET								m_serverSocket;
	std::map<SOCKET, IRequestHandler *> m_clients;
	ICryptoAlgorithm					*m_cryptoAlgorithm;

	/**
	 * @brief Contructor
	 */
	Communicator();
public:
	/**
	 * @brief Returns communicator instance
	 * @return Communicator instance
	*/
	static Communicator &Instance();
	/// Delete copy constructor
	Communicator( const Communicator & ) = delete;
	/// delete = operator
	void operator=( const Communicator & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~Communicator();

	/**
	 * @brief Starts a server request handler
	 */
	void startHandleRequests();

	/**
	 * @brief Returns user's socket IRequestHandler
	 * @param username User's username
	 * @return User's socket and IRequestHandler
	 */
	std::pair<const SOCKET, IRequestHandler *> &getUser( const std::string &username );
	
	/**
	 * @brief Sends crypted message to an user
	 * @param socket User to send message
	 * @param message Message to encrypt and send
	 */
	void sendMessage( const SOCKET socket, const std::vector<uint8_t> &message );
private:
	/**
	 * @brief Setups bind and listen for a clients
	 */
	void bindAndListen();
	/**
	 * @brief Handles new client
	 * @param clientSocket Socket to contact with client
	 */
	void handleNewClient( SOCKET clientSocket );

	RequestInfo recvRequestInfo( SOCKET socket ) const;

	/**
	 * @brief Reads value with type from socket and returns it
	 * @tparam T type to read
	 * @param socket Socket to read from
	 * @return Value
	 */
	template<typename T> static T readFromSocket( SOCKET socket ) {
		T value;
		if ( ::recv( socket, reinterpret_cast<char *>( &value ), sizeof( T ), 0 ) <= 0 ) throw std::exception( "Error while recieving from socket" );
		return value;
	}
	/**
	 * @brief Reads bytes from socket and returns it
	 * @param socket Socket to read from
	 * @param size Number of bytes to read
	 * @return Read bytes
	 */
	static std::vector<uint8_t> readFromSocketWithSize( SOCKET socket, size_t size );
};

#endif // COMMUNICATOR_H