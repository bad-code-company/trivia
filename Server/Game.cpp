#include "Game.h"
#include "Server.h"
#include "RoomManager.h"
#include <algorithm>

std::mutex Game::m_leaveGameMtx;

Game::Game( const Room &room, const std::vector<Question> &questions ) : m_roomId( room.getMetadata().m_id ), m_questions( questions ) {
	for ( auto &&user : room.getAllUsers() ) {
		this->m_players.insert( std::make_pair( LoggedUser( user ), GameData{ Question( this->m_questions.front() ), 0, 0, 0 } ) );
	}
	Server::Instance().database()->addNewGame();
}

Game::~Game() {}

Question Game::getQuestionForUser( const LoggedUser &user ) {
	if ( this->m_players.empty() || this->m_players.find( user ) == this->m_players.end() ) throw std::exception( "User is not into the game" );
	// check is there are questions for user
	uint32_t answersCount = this->m_players.at( user ).m_correctAnswerCount + this->m_players.at( user ).m_wrongAnswerCount;
	if ( answersCount == this->m_questions.size() ) throw std::exception( "No more questions" );
	// start timer
	this->m_players.at( user ).m_timeCounter = std::chrono::system_clock::now();
	return this->m_questions[answersCount];
}

void Game::submitAnswer( const LoggedUser &user, uint32_t answerId ) {
	if ( this->m_players.empty() || this->m_players.find( user ) == this->m_players.end() ) throw std::exception( "User is not into the game" );
	uint32_t answersCount = this->m_players.at( user ).m_correctAnswerCount + this->m_players.at( user ).m_wrongAnswerCount;
	if ( answersCount == this->m_questions.size() ) throw std::exception( "No more questions" );
	// check answer id
	else if ( answerId > 4 || answerId < 0 ) throw std::exception( "Invalid answer id" );
	// update average answer time (current + next / 2)
	if ( this->m_players.at( user ).m_averageAnswerTime == 0.f ) this->m_players.at( user ).m_averageAnswerTime = std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - this->m_players.at( user ).m_timeCounter ).count() / 1000.f;
	else this->m_players.at( user ).m_averageAnswerTime = ( this->m_players.at( user ).m_averageAnswerTime + std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now() - this->m_players.at( user ).m_timeCounter ).count() / 1000.f ) / 2.f;
	// if answer right
	if ( answerId != 4 && this->m_players.at( user ).m_currentQuestion.getPossibleAnswers().at( answerId ) == this->m_players.at( user ).m_currentQuestion.getCorrectAnswer() )++this->m_players.at( user ).m_correctAnswerCount;
	else ++this->m_players.at( user ).m_wrongAnswerCount;
	// update question
	answersCount = this->m_players.at( user ).m_correctAnswerCount + this->m_players.at( user ).m_wrongAnswerCount;
	if ( answersCount < this->m_questions.size() ) this->m_players.at( user ).m_currentQuestion = Question( this->m_questions[answersCount] );
}

uint32_t Game::getCorrectAnswerId( const LoggedUser &user ) const {
	for ( auto &&answer : this->m_players.at( user ).m_currentQuestion.getPossibleAnswers() ) {
		if ( answer.second == this->m_players.at( user ).m_currentQuestion.getCorrectAnswer() ) return answer.first;
	}
	return 0;
}

void Game::removePlayer( const LoggedUser &user ) {
	std::unique_lock<std::mutex> leaveGameMtx( Game::m_leaveGameMtx );
	if ( this->m_players.empty() || this->m_players.find( user ) == this->m_players.end() ) throw std::exception( "User is not into the game" );
	// remove user from the game
	this->m_players.erase( user );
	// remove user from the room
	RoomManager::Instance().getRoom( this->m_roomId ).removeUser( user );
	leaveGameMtx.unlock();
}

std::vector<LoggedUser> Game::getUserList() const {
	std::vector<LoggedUser> userList;
	std::unique_lock<std::mutex> leaveGameMtx( Game::m_leaveGameMtx );
	for ( auto &&player : this->m_players ) {
		userList.push_back( player.first );
	}
	leaveGameMtx.unlock();
	return userList;
}

std::vector<PlayerResults> Game::getGameResults() const {
	std::vector<PlayerResults> gameResults;
	std::unique_lock<std::mutex> leaveGameMtx( Game::m_leaveGameMtx );
	for ( auto &&player : this->m_players ) {
		if ( player.second.m_correctAnswerCount + player.second.m_wrongAnswerCount < this->m_questions.size() ) throw std::exception( "Game is not ended" );
		gameResults.push_back( PlayerResults{ player.first.getUsername(), player.second.m_correctAnswerCount, player.second.m_wrongAnswerCount, player.second.m_averageAnswerTime } );
	}
	leaveGameMtx.unlock();
	return gameResults;
}

uint32_t Game::getRoomId() const {
	return this->m_roomId;
}

GameData Game::getUserGameData( const LoggedUser &user ) const {
	return this->m_players.at( user );
}
