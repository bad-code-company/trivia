#ifndef GAME_H
#define GAME_H

#include "LoggedUser.h"
#include "Question.h"
#include "Room.h"
#include "Responses.h"
#include <cstdint>
#include <vector>
#include <map>
#include <chrono>
#include <mutex>

struct GameData {
	Question									m_currentQuestion;
	uint32_t									m_correctAnswerCount;
	uint32_t									m_wrongAnswerCount;
	float										m_averageAnswerTime;
	std::chrono::system_clock::time_point		m_timeCounter;
};

class Game {
private:
	/// field
	uint32_t m_roomId;
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;

	static std::mutex m_leaveGameMtx;
public:
	/**
	 * @brief Constructor
	 * @param room Game's room
	 */
	Game( const Room &room, const std::vector<Question> &questions );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~Game();

	/**
	 * @brief Returns next question for user
	 * @param user User to get a next question
	 * @return Question for user
	 */
	Question getQuestionForUser( const LoggedUser &user );
	/**
	 * @brief Submits answer for a current question
	 * @param user User to submit an answer
	 * @param answerId Answer id
	 */
	void submitAnswer( const LoggedUser &user, uint32_t answerId );
	/**
	 * @brief Return current right answer id
	 * @param user User to check answer id
	 * @return Current right answer id
	 */
	uint32_t getCorrectAnswerId( const LoggedUser &user) const;
	/**
	 * @brief Removes user from the game
	 * @param user User to remove the game
	 */
	void removePlayer( const LoggedUser &user );
	/**
	 * @brief Returns user list
	 * @return User list
	 */
	std::vector<LoggedUser> getUserList() const;
	/**
	 * @brief Returns game results
	 * @return Vector of results of every single player
	 */
	std::vector<PlayerResults> getGameResults() const;
	/**
	 * @brief Returns room's id
	 * @return Room's id
	 */
	uint32_t getRoomId() const;
	/**
	 * @brief Returns user's gamedata
	 * @param user User to get a data
	 * @return User's gamedata
	 */
	GameData Game::getUserGameData( const LoggedUser &user ) const;
};

#endif // GAME_H
