#include "GameManager.h"
#include "Server.h"

GameManager &GameManager::Instance() {
	static GameManager instance;
	return instance;
}

GameManager::GameManager() {}

GameManager::~GameManager() {}

Game &GameManager::createGame( const Room &room ) {
	this->m_games.push_back( Game( room, Server::Instance().database()->getQuestions( room.getMetadata().m_numOfQuestionsInGame ) ) );
	return this->m_games.back();
}

Game &GameManager::getGame( const uint32_t roomId ) {
	for ( auto &&game : this->m_games ) {
		if ( game.getRoomId() == roomId ) return game;
	}
	throw std::exception( "No game with this room id" );
}

void GameManager::deleteGame( const std::string &username ) {
	for ( auto it = this->m_games.begin(); it != this->m_games.end(); ++it ) {
		// is user into the game
		auto userList = it->getUserList();
		if ( std::find( userList.begin(), userList.end(), username ) != userList.end() ) {
			this->m_games.erase( it );
			break;
		}
	}
}

std::vector<Game> &GameManager::getGames() {
	return this->m_games;
}
