#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include "Game.h"
#include "Room.h"
#include <vector>

class GameManager {
private:
	std::vector<Game> m_games;
	
	/**
	 * @brief Constructor
	 */
	GameManager();
public:
	/**
	 * @brief Returns Game manager factory
	 * @return Game manager instance
	 */
	static GameManager &Instance();
	/// Delete copy constructor
	GameManager( const GameManager & ) = delete;
	/// delete = operator
	void operator=( const GameManager & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~GameManager();

	/**
	 * @brief Creates a game and returns it
	 * @param room Room to create a game
	 * @return Game
	 */
	Game &createGame( const Room &room );
	/**
	 * @brief Returns game reference
	 * @param roomId Room id
	 * @return Game reference
	 */
	Game &getGame( const uint32_t roomId );
	/**
	 * @brief Deletes game by username
	 * @param username User's username
	 */
	void deleteGame( const std::string &username );
	/**
	 * @brief Returns games' vector
	 * @return Games' vector
	 */
	std::vector<Game> &getGames();
};

#endif // GAME_MANAGER_H
