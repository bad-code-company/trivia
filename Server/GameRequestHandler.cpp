#include "GameRequestHandler.h"
#include "GameManager.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "StatisticsManager.h"

GameRequestHandler::GameRequestHandler( Game &game, const LoggedUser &user ) : IRequestHandler(), m_game( game ), m_user( user ) {}

GameRequestHandler::~GameRequestHandler() {}

bool GameRequestHandler::isRequestRelevant( const RequestInfo &requestInfo ) const {
	return requestInfo.m_code == RequestId::LEAVE_GAME_ID || requestInfo.m_code == RequestId::GET_QUESTION_ID || requestInfo.m_code == RequestId::SUBMIT_ANSWER_ID || requestInfo.m_code == RequestId::GET_GAME_RESULT;
}

RequestResult GameRequestHandler::handleRequest( const RequestInfo &requestInfo ) {
	switch ( requestInfo.m_code ) {
		case RequestId::LEAVE_GAME_ID:
			return this->leaveGame( requestInfo );
			break;
		case RequestId::GET_QUESTION_ID:
			return this->getQuestion( requestInfo );
			break;
		case RequestId::SUBMIT_ANSWER_ID:
			return this->submitAnswer( requestInfo );
			break;
		case RequestId::GET_GAME_RESULT:
			return this->getGameResults( requestInfo );
			break;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Invalid request" } ), this };
}

std::string GameRequestHandler::getUsername() const {
	return this->m_user.getUsername();
}

RequestResult GameRequestHandler::getQuestion( const RequestInfo &requestInfo ) const {
	uint32_t status = 1;
	std::string question;
	std::map<uint32_t, std::string> answers;
	try {
		Question questionObj = this->m_game.getQuestionForUser( this->m_user );
		question = questionObj.getQuestion();
		answers = std::map<uint32_t, std::string>( questionObj.getPossibleAnswers() );
	} catch ( const std::exception & ) {
		status = 0;
		question.clear();
		answers.clear();
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetQuestionResponse{ status, question, answers } ), const_cast<GameRequestHandler *>( this ) };
}

RequestResult GameRequestHandler::submitAnswer( const RequestInfo &requestInfo ) {
	SubmitAnswerRequest submitAnswerRequest = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest( requestInfo.m_buffer );
	uint32_t status = 1, correctAnswerId = 0;
	try {
		correctAnswerId = this->m_game.getCorrectAnswerId( this->m_user );
		this->m_game.submitAnswer( this->m_user, submitAnswerRequest.m_answerId );
	} catch ( const std::exception & ) {
		status = 0;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( SubmitAnswerResponse{ status, correctAnswerId } ), this };
}

RequestResult GameRequestHandler::getGameResults( const RequestInfo &requestInfo ) const {
	uint32_t status = 1;
	std::vector<PlayerResults> gameResults;
	try {
		gameResults = this->m_game.getGameResults();
	} catch ( const std::exception & ) {
		status = 0;
		gameResults.clear();
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetGameResultsResponse{ status, gameResults } ), const_cast<GameRequestHandler *>( this ) };
}

RequestResult GameRequestHandler::leaveGame( const RequestInfo &requestInfo ) {
	try {
		// update user statistics
		auto userGameData = this->m_game.getUserGameData( this->m_user );
		StatisticsManager::Instance().updateUserStatistics( this->m_user, userGameData.m_correctAnswerCount + userGameData.m_wrongAnswerCount, userGameData.m_correctAnswerCount, userGameData.m_averageAnswerTime );
		// if no users then remove the room
		if ( this->m_game.getUserList().size() == 1 ) {
			// auto roomId = this->m_game.getRoomId();
			RoomManager::Instance().deleteRoom( this->m_game.getRoomId() );
			GameManager::Instance().deleteGame( this->m_user.getUsername() );
		} else {
			this->m_game.removePlayer( this->m_user );
		}
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( LeaveGameResponse{ 1 } ), RequestHandlerFactory::Instance().createMenuRequestHandler( this->m_user ) };
	} catch ( const std::exception & ) {}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( LeaveGameResponse{ 0 } ), this };
}
