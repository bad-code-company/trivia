#ifndef GAME_REQUEST_HANDLER_H
#define GAME_REQUEST_HANDLER_H

#include "IRequestHandler.h"
#include "Game.h"
#include "LoggedUser.h"

class GameRequestHandler : public IRequestHandler {
private:
	/// fields
	Game &m_game;
	LoggedUser m_user;
public:
	/**
	 * @brief Constructor
	 * @param game Game
	 * @param user Handler's user
	 */
	GameRequestHandler( Game &game, const LoggedUser &user );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~GameRequestHandler();

	/**
	 * @brief Returns is next request is relevant according to current
	 * @param requestInfo Next request info
	 * @return Is next request is relevant according to current
	 */
	virtual bool isRequestRelevant( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handels next request
	 * @param requestInfo Next request info
	 * @return Request result
	 */
	virtual RequestResult handleRequest( const RequestInfo &requestInfo );
	/**
	 * @brief Returns user's username
	 * @return User's username
	 */
	virtual std::string getUsername() const;
private:
	/**
	 * @brief Handles 'get question' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getQuestion( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handles 'submit answers' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult submitAnswer( const RequestInfo &requestInfo );
	/**
	 * @brief Handles 'get game results' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getGameResults( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handles 'leave game' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult leaveGame( const RequestInfo &requestInfo );
};

#endif // GAME_REQUEST_HANDLER_H
