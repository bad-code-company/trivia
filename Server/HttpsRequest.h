#ifndef HTTPS_REQUEST_H
#define HTTPS_REQUEST_H

#include <windows.h>
#include <winhttp.h>
#pragma comment( lib, "winhttp.lib" )
#include <string>

class HttpsRequest {
private:
	/// fields
	std::wstring m_domain;
public:
	/**
	 * @brief Builds a https request object
	 * @param domain Server domain
	 */
	HttpsRequest( const std::string &domain ) : m_domain( std::wstring( domain.begin(), domain.end() ) ) {}
	/**
	 * @brief Virtual destructor
	 */
	virtual ~HttpsRequest() {}

	/**
	 * @brief Performs a get request
	 * @param url Request url
	 * @return Page's body of a request
	 */
	std::string get( const std::string &url ) {
		HINTERNET session = nullptr, connect = nullptr, request = nullptr;
		std::string body;
		session = ::WinHttpOpen( nullptr, WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0 );
		if ( session ) {
			connect = ::WinHttpConnect( session, this->m_domain.c_str(), INTERNET_DEFAULT_HTTPS_PORT, 0 );
			if ( connect ) {
				request = ::WinHttpOpenRequest( connect, L"GET", std::wstring( url.begin(), url.end() ).c_str(), NULL, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE );
				if ( request ) {
					if ( ::WinHttpSendRequest( request, nullptr, -1, NULL, 0, 0, 0 ) ) {
						::WinHttpReceiveResponse( request, NULL );
						DWORD size = 0;
						::WinHttpQueryDataAvailable( request, &size );
						body.resize( size );
						::WinHttpReadData( request, const_cast<char *>( body.data() ), size, nullptr );
					}
					::WinHttpCloseHandle( request );
				}
				::WinHttpCloseHandle( connect );
			}
			::WinHttpCloseHandle( session );
		}
		return body;
	}
};

#endif // HTTPS_REQUEST_H
