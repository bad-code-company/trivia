#ifndef ICRYPTO_ALGORITHM_H
#define ICRYPTO_ALGORITHM_H

#include <cstdint>
#include <vector>

class ICryptoAlgorithm {
protected:
	/**
	 * @brief Default constructor
	 */
	ICryptoAlgorithm() = default;
public:
	/// Delete copy constructor
	ICryptoAlgorithm( const ICryptoAlgorithm & ) = delete;
	/// Delete = operator
	void operator=( const ICryptoAlgorithm & ) = delete;
	/**
	 * @brief Default virtual destruct5or
	 */
	virtual ~ICryptoAlgorithm() = default;

	/**
	 * @brief Encrypts the buffer and returns it
	 * @param buffer Buffer to encrypt
	 * @return Encrypted buffer
	 */
	virtual std::vector<uint8_t> encrypt( const std::vector<uint8_t> &buffer ) = 0;
	/**
	 * @brief Decrypts the buffer and returns it
	 * @param buffer Buffer to decrypt
	 * @return Decrypted buffer
	 */
	virtual std::vector<uint8_t> decrypt( const std::vector<uint8_t> &buffer ) = 0;
};

#endif // ICRYPTO_ALGORITHM_H
