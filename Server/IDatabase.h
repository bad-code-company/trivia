#ifndef IDATABASE_H
#define IDATABASE_H

#include <string>
#include <list>
#include <vector>
#include "Question.h"

class IDatabase {
protected:
	/**
	 * @brief Default constructor
	 */
	IDatabase() = default;
public:
	/// Delete copy constructor
	IDatabase( const IDatabase & ) = delete;
	/// delete = operator
	void operator=( const IDatabase & ) = delete;
	/**
	 * @brief Default virtual destructor
	 */
	virtual ~IDatabase() = default;
	/**
	 * @brief Checks does user exist
	 * @param username User name to find
	 * @return Does user exist
	 */
	virtual bool doesUserExist( const std::string &username ) const = 0;
	/**
	 * @brief Checks does password match to user (according to user name)
	 * @param username User's name
	 * @param password Password to check
	 * @return Does password match to user
	 */
	virtual bool doesPasswordMatch( const std::string &username, const std::string &password ) const = 0;
	/**
	 * @brief Adds new user to a database
	 * @param username User's name to add
	 * @param password User's password
	 * @param email User's email
	 */
	virtual void addNewUser( const std::string &username, const std::string &password, const std::string &email ) = 0;

	/**
	 * @brief Returns 'quantity' questions from the database
	 * @param quantity Question quantity
	 * @return 'Quantity' questions from the database
	 */
	virtual std::vector<Question> getQuestions( const int quantity ) const = 0;
	/**
	 * @brief Returns user's average answer time
	 * @param username User's username
	 * @return User's average answer time
	 */
	virtual float getPlayerAverageAnswerTime( const std::string &username ) const = 0;
	/**
	 * @brief Returns number of correct answers of the user
	 * @param username User's username
	 * @return Number of correct answers of the user
	 */
	virtual int getNumOfCorrectAnswers( const std::string &username ) const = 0;
	/**
	 * @brief Returns number of answers of the user
	 * @param username User's username
	 * @return Number of answers of the user
	 */
	virtual int getNumOfTotalAnswers( const std::string &username ) const = 0;
	/**
	 * @brief Returns number of games of the user
	 * @param username User's username
	 * @return Number of games of the user
	 */
	virtual int getNumOfPlayerGames( const std::string &username ) const = 0;
	/**
	 * @brief Returns vector of 5 best players
	 * @return Vector of 5 best players
	 */
	virtual std::vector<std::string> getBestPlayers() const = 0;

	/**
	 * @brief Adds new game to database
	 */
	virtual void addNewGame() = 0;
	/**
	 * @brief Updates user statistics
	 * @param username User to update stats
	 * @param gamesPlayed New games played value
	 * @param totalAnswers New total answers value
	 * @param correctAnswers New correct answers value
	 * @param answerTime New answer time value
	 */
	virtual void updateUserStatistics( const std::string &username, const int gamesPlayed, const int totalAnswers, const int correctAnswers, const float answerTime ) = 0;
private:
	/**
	 * @brief Adds new questions to db
	 * @param quantity Number of questions to add
	 */
	virtual void addQuestionsToDatabase( uint32_t quantity ) = 0;
};

#endif // IDATABASE_H
