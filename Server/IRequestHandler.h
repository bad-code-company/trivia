#ifndef IREQUEST_HANDLER_H
#define IREQUEST_HANDLER_H

#include "Requests.h"

class IRequestHandler {
public:
	/**
	 * @brief Default constructor
	 */
	IRequestHandler() = default;
	/**
	 * @brief Default virtual destructor
	 */
	virtual ~IRequestHandler() = default;
	/**
	 * @brief Returns is next request is relevant according to current
	 * @param requestInfo Next request info
	 * @return Is next request is relevant according to current
	 */
	virtual bool isRequestRelevant( const RequestInfo &requestInfo ) const = 0;
	/**
	 * @brief Handels next request
	 * @param requestInfo Next request info
	 * @return Request result
	 */
	virtual RequestResult handleRequest( const RequestInfo &requestInfo ) = 0;
	/**
	 * @brief Returns user's username
	 * @return User's username
	 */
	virtual std::string getUsername() const = 0;
};

#endif // IREQUEST_HANDLER_H