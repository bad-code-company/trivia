#include "JsonRequestPacketDeserializer.h"
#include <nlohmann/json.hpp>

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest( const std::vector<uint8_t> &buffer ) {
	nlohmann::json json = nlohmann::json::parse( buffer.begin(), buffer.end() );
	return LoginRequest{ json["username"], json["password"] };
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest( const std::vector<uint8_t> &buffer ) {
	nlohmann::json json = nlohmann::json::parse( buffer.begin(), buffer.end() );
	return SignupRequest{ json["username"], json["password"], json["email"] };
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest( const std::vector<uint8_t> &buffer ) {
	nlohmann::json json = nlohmann::json::parse( buffer.begin(), buffer.end() );
	return GetPlayersInRoomRequest{ json["roomId"].get<uint32_t>() };
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest( const std::vector<uint8_t> &buffer ) {
	nlohmann::json json = nlohmann::json::parse( buffer.begin(), buffer.end() );
	return JoinRoomRequest{ json["roomId"].get<uint32_t>() };
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest( const std::vector<uint8_t> &buffer ) {
	nlohmann::json json = nlohmann::json::parse( buffer.begin(), buffer.end() );
	return CreateRoomRequest{ json["roomName"], json["maxUsers"].get<uint32_t>(), json["questionCount"].get<uint32_t>(), json["answerTimeout"].get<uint32_t>() };
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest( const std::vector<uint8_t> &buffer ) {
	nlohmann::json json = nlohmann::json::parse( buffer.begin(), buffer.end() );
	return SubmitAnswerRequest{ json["answerId"].get<uint32_t>() };
}