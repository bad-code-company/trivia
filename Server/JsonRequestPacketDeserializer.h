#ifndef JSON_REQUEST_PACKET_DESERIALIZER_H
#define JSON_REQUEST_PACKET_DESERIALIZER_H

#include <cstdint>
#include <vector>
#include "Requests.h"

class JsonRequestPacketDeserializer {
public:
	/**
	 * @brief Deserialize login request
	 * @param buffer A bytes of the request
	 * @return Login request into the struct
	 */
	static LoginRequest deserializeLoginRequest( const std::vector<uint8_t> &buffer );
	/**
	 * @brief Deserialize signup request
	 * @param buffer A bytes of the request
	 * @return Signup request into the struct
	 */
	static SignupRequest deserializeSignupRequest( const std::vector<uint8_t> &buffer );

	/**
	 * @brief Deserialize 'get players in room' request
	 * @param buffer A bytes of the request
	 * @return 'Get players in room' request into the struct
	 */
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest( const std::vector<uint8_t> &buffer );
	/**
	 * @brief Deserialize 'join room' request
	 * @param buffer A bytes of the request
	 * @return 'Join room' request into the struct
	 */
	static JoinRoomRequest deserializeJoinRoomRequest( const std::vector<uint8_t> &buffer );
	/**
	 * @brief Deserialize 'create room' request
	 * @param buffer A bytes of the request
	 * @return 'Create room' request into the struct
	 */
	static CreateRoomRequest deserializeCreateRoomRequest( const std::vector<uint8_t> &buffer );
	/**
	 * @brief Deserialize 'submit answer' request
	 * @param buffer A bytes of the request
	 * @return 'Submit answer' request into the struct
	 */
	static SubmitAnswerRequest deserializeSubmitAnswerRequest( const std::vector<uint8_t> &buffer );
};

#endif // JSON_REQUEST_PACKET_DESERIALIZER_H
