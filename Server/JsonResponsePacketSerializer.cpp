#include "JsonResponsePacketSerializer.h"
#include "Room.h"
#include <nlohmann/json.hpp>

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const ErrorResponse &error ) {
    nlohmann::json j;
    j["message"] = error.m_message;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::ERROR_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const LoginResponse &login ) {
    nlohmann::json j;
    j["status"] = login.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::LOGIN_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const SignupResponse &signup ) {
    nlohmann::json j;
    j["status"] = signup.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::SIGNUP_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const LogoutResponse &logout ) {
    nlohmann::json j;
    j["status"] = logout.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::LOGOUT_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetRoomsResponse &getRooms ) {
    nlohmann::json j;
    j["status"] = getRooms.m_status;
    std::vector<nlohmann::json> jsons;
    for ( auto &&room : getRooms.m_rooms ) {
        nlohmann::json innerJson;
        innerJson["id"]                     = room.m_id;
        innerJson["name"]                   = room.m_name;
        innerJson["maxPlayers"]             = room.m_maxPlayers;
        innerJson["numOfQuestionsInGame"]   = room.m_numOfQuestionsInGame;
        innerJson["timePerQuestion"]        = room.m_timePerQuestion;
        innerJson["isActive"]               = room.m_isActive;
        jsons.push_back( innerJson );
    }
    j["rooms"] = jsons;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_ROOMS_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetPlayersInRoomResponse &getPlayersInRoom ) {
    nlohmann::json j;
    j["players"] = getPlayersInRoom.m_players;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_PLAYERS_IN_ROOM_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetHighScoreResponse &getHighScore ) {
    nlohmann::json j;
    j["status"] = getHighScore.m_status;
    j["statistics"] = getHighScore.m_statistics;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_HIGH_SCORE_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetPersonalStatsResponse &getPersonalStats ) {
    nlohmann::json j;
    j["status"]     = getPersonalStats.m_status;
    j["statistics"] = getPersonalStats.m_statistics;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_PERSONAL_STATS_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const JoinRoomResponse &joinRoom ) {
    nlohmann::json j;
    j["status"] = joinRoom.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::JOIN_ROOM_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const CreateRoomResponse &createRoom ) {
    nlohmann::json j;
    j["status"] = createRoom.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::CREATE_ROOM_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const CloseRoomResponse &closeRoom ) {
    nlohmann::json j;
    j["status"] = closeRoom.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::CLOSE_ROOM_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const StartGameResponse &startGame ) {
    nlohmann::json j;
    j["status"] = startGame.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::START_GAME_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetRoomStateResponse &getRoomState ) {
    nlohmann::json j;
    j["status"]         = getRoomState.m_status;
    j["hasGameBegun"]   = getRoomState.m_hasGameBegun;
    j["players"]        = getRoomState.m_players;
    j["questionCount"]  = getRoomState.m_questionCount;
    j["answerTimeout"]  = getRoomState.m_answerTimeout;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_ROOM_STATE_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const LeaveRoomResponse &leaveRoom ) {
    nlohmann::json j;
    j["status"] = leaveRoom.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::LEAVE_ROOM_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const LeaveGameResponse &leaveGame ) {
    nlohmann::json j;
    j["status"] = leaveGame.m_status;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::LEAVE_GAME_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetQuestionResponse &getQuestion ) {
    nlohmann::json j;
    j["status"] = getQuestion.m_status;
    std::string question( getQuestion.m_question );
    std::replace( question.begin(), question.end(), '\"', '\'' );
    j["question"] = question;
    std::map<uint32_t, std::string> answers( getQuestion.m_answers );
    // insert map into json vector
    std::vector<nlohmann::json> jsonAnswers;
    for ( auto &&answer : answers ) {
        std::replace( answer.second.begin(), answer.second.end(), '\"', '\'' );
        nlohmann::json answerJson;
        answerJson[std::to_string( answer.first )] = answer.second;
        jsonAnswers.push_back( answerJson );
    }
    j["answers"] = jsonAnswers;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_QUESTION_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const SubmitAnswerResponse &submitAnswer ) {
    nlohmann::json j;
    j["status"]             = submitAnswer.m_status;
    j["correctAnswerId"]    = submitAnswer.m_correctAnswerId;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::SUBMIT_ANSWER_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::serializeResponse( const GetGameResultsResponse &getGameResults ) {
    nlohmann::json j;
    j["status"] = getGameResults.m_status;
    std::vector<nlohmann::json> jsons;
    for ( auto &&result : getGameResults.m_results ) {
        nlohmann::json innerJson;
        innerJson["username"]           = result.m_username;
        innerJson["correctAnswerCount"] = result.m_correctAnswerCount;
        innerJson["wrongAnswerCount"]   = result.m_wrongAnswerCount;
        innerJson["averageAnswerTime"]  = result.m_averageAnswerTime;
        jsons.push_back( innerJson );
    }
    j["results"] = jsons;
    std::string buffer = j.dump();
    return JsonResponsePacketSerializer::generalSerialize( ResponseId::GET_GAME_RESULT_ID, std::vector<uint8_t>( buffer.begin(), buffer.end() ) );
}

std::vector<uint8_t> JsonResponsePacketSerializer::generalSerialize( const ResponseId id, const std::vector<uint8_t> &jsonBuffer ) {
    std::vector<uint8_t> buffer; // The buffer to return
    buffer.push_back( static_cast<uint8_t>( id ) );
    // Adding the 4 bytes message length to a 1 byte vector by taking it apart into 4 one-byte pieces.
    for ( int i = 3 * BYTE_LENGTH; i >= 0; i -= BYTE_LENGTH ) {
        buffer.push_back( ( jsonBuffer.size() >> i ) & HEXADECIMAL_255 );
    }
    buffer.insert( buffer.end(), jsonBuffer.begin(), jsonBuffer.end() );
    return buffer;
}
