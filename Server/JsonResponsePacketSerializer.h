#pragma once
#include <cstdint>
#include <vector>
#include "Responses.h"

#define BYTE_LENGTH 8
#define HEXADECIMAL_255 0xFF

class JsonResponsePacketSerializer {
public:
	/**
	 * @brief A function that serializes a response struct into a buffer of bytes.
	 * @param error A response struct of error message.
	 * @return A buffer of serialized json error response.
	 */
	static std::vector<uint8_t> serializeResponse( const ErrorResponse &error );
	/**
	 * @brief A function that serializes a response struct into a buffer of bytes.
	 * @param login A response struct for login.
	 * @return A buffer of serialized json login response.
	 */
	static std::vector<uint8_t> serializeResponse( const LoginResponse &login );
	/**
	 * @brief A function that serializes a response struct into a buffer of bytes.
	 * @param signup A response struct for signup.
	 * @return A buffer of serialized json signup response.
	 */
	static std::vector<uint8_t> serializeResponse( const SignupResponse &signup );

	/**
	 * @brief Serializes a logout response into a buffer of bytes
	 * @param logout A logout response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const LogoutResponse &logout );
	/**
	 * @brief Serializes a 'get rooms' response into a buffer of bytes
	 * @param getRooms A 'get rooms' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetRoomsResponse &getRooms );
	/**
	 * @brief Serializes a 'get players in room' response into a buffer of bytes
	 * @param getPlayersInRoom A 'get players in room' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetPlayersInRoomResponse &getPlayersInRoom );
	/**
	 * @brief Serializes a 'get high score' response into a buffer of bytes
	 * @param getHighScore A 'get high score' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetHighScoreResponse &getHighScore );
	/**
	 * @brief Serializes a 'get personal stats' response into a buffer of bytes
	 * @param getPersonalStats A 'get personal stats' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetPersonalStatsResponse &getPersonalStats );
	/**
	 * @brief Serializes a 'join room' response into a buffer of bytes
	 * @param joinRoom A 'join room' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const JoinRoomResponse &joinRoom );
	/**
	 * @brief Serializes a 'create room' response into a buffer of bytes
	 * @param createRoom A 'create room' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const CreateRoomResponse &createRoom );

	/**
	 * @brief Serializes a 'close room' response into a buffer of bytes
	 * @param closeRoom A 'close room' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const CloseRoomResponse &closeRoom );
	/**
	 * @brief Serializes a 'start game' response into a buffer of bytes
	 * @param startGame A 'start game' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const StartGameResponse &startGame );
	/**
	 * @brief Serializes a 'get room state' response into a buffer of bytes
	 * @param getRoomState A 'get room state' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetRoomStateResponse &getRoomState );
	/**
	 * @brief Serializes a 'leave room' response into a buffer of bytes
	 * @param leaveRoom A 'leave room' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const LeaveRoomResponse &leaveRoom );

	/**
	 * @brief Serializes a 'leave game' response into a buffer of bytes
	 * @param leaveGame A 'leave game' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const LeaveGameResponse &leaveGame );
	/**
	 * @brief Serializes a 'get question' response into a buffer of bytes
	 * @param getQuestion A 'get question' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetQuestionResponse &getQuestion );
	/**
	 * @brief Serializes a 'submit answer' response into a buffer of bytes
	 * @param submitAnswer A 'submit answer' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const SubmitAnswerResponse &submitAnswer );
	/**
	 * @brief Serializes a 'get game results' response into a buffer of bytes
	 * @param getGameResults A 'get game results' response struct
	 * @return A serizalized response
	 */
	static std::vector<uint8_t> serializeResponse( const GetGameResultsResponse &getGameResults );
private:
	/**
	 * @brief A function that gets the 3 fields of a buffer and creates it.
	 * @param id The response message code.
	 * @param dataLength The length of the passed data from the buffer.
	 * @param message The message/data to pass.
	 * @return A buffer built from the given fields.
	 */
	static std::vector<uint8_t> generalSerialize( const ResponseId id, const std::vector<uint8_t> &jsonBuffer );
};

