#ifndef LOGGED_USER_H
#define LOGGED_USER_H

#include <string>

class LoggedUser {
private:
	/// fields
	std::string m_username;
public:
	/**
	 * @brief Constructor
	 * @param username User's name
	 */
	LoggedUser( const std::string &username );
	/**
	 * @brief Copy constructor
	 * @param other User to copy
	 */
	LoggedUser( const LoggedUser &other );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~LoggedUser();

	/**
	 * @brief Returns user's name
	 * @return User's name
	 */
	std::string getUsername() const;

	/**
	 * @brief Equality operator checks a equality of two logged users
	 * @param other Other user to compare
	 * @return Does two users equal each other
	 */
	bool operator==( const LoggedUser &other ) const;
	/**
	 * @brief Equality operator checks a equality of username of user
	 * @param username User name to compare
	 * @return Does username equal to user's username
	 */
	bool operator==( const std::string &username ) const;
	/**
	 * @brief Less operator checks is a current username less than another
	 * @param other Other user to compare
	 * @return Is a current username less than another
	 */
	bool operator<( LoggedUser const &other ) const;
};

#endif // LOGGED_USER_H
