#include "LoginManager.h"
#include "SqliteDatabase.h"
#include "Server.h"
#include <exception>

std::mutex LoginManager::m_logoutMtx;

LoginManager &LoginManager::Instance() {
	static LoginManager instance;
	return instance;
}

LoginManager::LoginManager() {}

LoginManager::~LoginManager() {}

void LoginManager::signup( const std::string &username, const std::string &password, const std::string &email ) {
	if ( !Server::Instance().database() ) throw std::exception( "Database is nullptr" );
	if ( Server::Instance().database()->doesUserExist( username ) ) throw std::exception( "User already exists" );
	Server::Instance().database()->addNewUser( username, password, email );
}

void LoginManager::login( const std::string &username, const std::string &password ) {
	if ( !Server::Instance().database() ) throw std::exception( "Database is nullptr" );
	if ( std::find( this->m_loggedUsers.begin(), this->m_loggedUsers.end(), username ) != this->m_loggedUsers.end() ) throw std::exception( "User is already logged in" );
	if ( !Server::Instance().database()->doesUserExist( username ) ) throw std::exception( "User does not exist" );
	if ( !Server::Instance().database()->doesPasswordMatch( username, password ) ) throw std::exception( "Invalid password" );
	this->m_loggedUsers.push_back( LoggedUser( username ) );
}

void LoginManager::logout( const std::string &username ) {
	std::unique_lock<std::mutex> logoutLock( LoginManager::m_logoutMtx );
	if ( this->m_loggedUsers.empty() || std::find( this->m_loggedUsers.begin(), this->m_loggedUsers.end(), username ) == this->m_loggedUsers.end() ) throw std::exception( "User is not logged in" );
	// remove from vector
	this->m_loggedUsers.erase( std::find( this->m_loggedUsers.begin(), this->m_loggedUsers.end(), username ) );
	logoutLock.unlock();
}