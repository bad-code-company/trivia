#ifndef LOGIN_MANAGER_H
#define LOGIN_MANAGER_H

#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>
#include <mutex>

class LoginManager {
private:
	std::vector<LoggedUser> m_loggedUsers;

	/**
	 * @brief Constructor
	 */
	LoginManager();

	static std::mutex m_logoutMtx; // mutex
public:
	/**
	 * @brief Returns login manager factory
	 * @return Login manager instance
	 */
	static LoginManager &Instance();
	/// Delete copy constructor
	LoginManager( const LoginManager & ) = delete;
	/// delete = operator
	void operator=( const LoginManager & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~LoginManager();

	/**
	 * @brief Adds new user to database
	 * @param username New user's name
	 * @param password User's password
	 * @param email User's e-mail
	 */
	void signup( const std::string &username, const std::string &password, const std::string &email );
	/**
	 * @brief Tries to log in to an account of user
	 * @param username User's name
	 * @param password User's password
	 */
	void login( const std::string &username, const std::string &password );
	/**
	 * @brief Tries to log out of user account
	 * @param username User's name
	 */
	void logout( const std::string &username );
};

#endif // LOGIN_MANAGER_H
