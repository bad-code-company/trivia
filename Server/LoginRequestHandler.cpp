#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include <exception>

LoginRequestHandler::LoginRequestHandler() : IRequestHandler() {}

LoginRequestHandler::~LoginRequestHandler() {}

bool LoginRequestHandler::isRequestRelevant( const RequestInfo &requestInfo ) const {
	return requestInfo.m_code == RequestId::LOGIN_ID || requestInfo.m_code == RequestId::SIGNUP_ID;
}

RequestResult LoginRequestHandler::handleRequest( const RequestInfo &requestInfo ) {
	switch ( requestInfo.m_code ) {
		case RequestId::LOGIN_ID:
			return this->login( requestInfo );
			break;
		case RequestId::SIGNUP_ID:
			return this->signup( requestInfo );
			break;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Invalid request" } ), this };
}

std::string LoginRequestHandler::getUsername() const {
	throw std::exception( "User is not logged in" );
}

RequestResult LoginRequestHandler::login( const RequestInfo &requestInfo ) {
	LoginRequest login_request = JsonRequestPacketDeserializer::deserializeLoginRequest( requestInfo.m_buffer );
	try {
		LoginManager::Instance().login( login_request.m_username, login_request.m_password );
	} catch ( const std::exception & ) {
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( LoginResponse{ 0 } ), this };
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( LoginResponse{ 1 } ),  RequestHandlerFactory::Instance().createMenuRequestHandler( LoggedUser( login_request.m_username ) ) };
}

RequestResult LoginRequestHandler::signup( const RequestInfo &requestInfo ) {
	SignupRequest signup_request = JsonRequestPacketDeserializer::deserializeSignupRequest( requestInfo.m_buffer );
	try {
		LoginManager::Instance().signup( signup_request.m_username, signup_request.m_password, signup_request.m_email );
	} catch ( const std::exception & ) {
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( SignupResponse{ 0 } ), this };
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( SignupResponse{ 1 } ), this };
}