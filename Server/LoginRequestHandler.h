#ifndef LOGIN_REQUEST_HANDLER_H
#define LOGIN_REQUEST_HANDLER_H

#include "IRequestHandler.h"

class LoginRequestHandler : public IRequestHandler {
public:
	/**
	 * @brief Constructor
	 */
	LoginRequestHandler();
	/**
	 * @brief Virtual destructor
	 */
	virtual ~LoginRequestHandler();

	/**
	 * @brief Returns is next request is relevant according to current
	 * @param requestInfo Next request info
	 * @return Is next request is relevant according to current
	 */
	virtual bool isRequestRelevant( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handels next request
	 * @param requestInfo Next request info
	 * @return Request result
	 */
	virtual RequestResult handleRequest( const RequestInfo &requestInfo );
	/**
	 * @brief Returns user's username
	 * @return User's username
	 */
	virtual std::string getUsername() const;
private:
	/**
	 * @brief Tries to enter to an user
	 * @param requestInfo Request's info
	 * @return Request's result
	 */
	RequestResult login( const RequestInfo &requestInfo );
	/**
	 * @brief Tries to sign up new user
	 * @param requestInfo Request's info
	 * @return Request's result
	 */
	RequestResult signup( const RequestInfo &requestInfo );
};

#endif // LOGIN_REQUEST_HANDLER_H
