#include "MenuRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"

MenuRequestHandler::MenuRequestHandler( const LoggedUser &user ) : m_user( user ) {}

MenuRequestHandler::~MenuRequestHandler() {}

bool MenuRequestHandler::isRequestRelevant( const RequestInfo &requestInfo ) const {
	return requestInfo.m_code == RequestId::LOGOUT_ID || requestInfo.m_code == RequestId::GET_ROOMS_ID || requestInfo.m_code == RequestId::GET_PLAYERS_IN_ROOM_ID || requestInfo.m_code == RequestId::GET_HIGH_SCORE_ID || requestInfo.m_code == RequestId::GET_PERSONAL_STATS_ID || requestInfo.m_code == RequestId::JOIN_ROOM_ID || requestInfo.m_code == RequestId::CREATE_ROOM_ID;
}

RequestResult MenuRequestHandler::handleRequest( const RequestInfo &requestInfo ) {
	switch ( requestInfo.m_code ) {
		case RequestId::LOGOUT_ID:
			return this->signout( requestInfo );
			break;
		case RequestId::GET_ROOMS_ID:
			return this->getRooms( requestInfo );
			break;
		case RequestId::GET_PLAYERS_IN_ROOM_ID:
			return this->getPlayersInRoom( requestInfo );
			break;
		case RequestId::GET_HIGH_SCORE_ID:
			return this->getHighScore( requestInfo );
			break;
		case RequestId::GET_PERSONAL_STATS_ID:
			return this->getPersonalStats( requestInfo );
			break;
		case RequestId::JOIN_ROOM_ID:
			return this->joinRoom( requestInfo );
			break;
		case RequestId::CREATE_ROOM_ID:
			return this->createRoom( requestInfo );
			break;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Invalid request" } ), this };
}

std::string MenuRequestHandler::getUsername() const {
	return this->m_user.getUsername();
}

RequestResult MenuRequestHandler::signout( const RequestInfo &requestInfo ) {
	try {
		LoginManager::Instance().logout( this->m_user.getUsername() );
	} catch ( const std::exception & ) {}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( LogoutResponse{ 1 } ), RequestHandlerFactory::Instance().createLoginRequestHandler() };
}

RequestResult MenuRequestHandler::getRooms( const RequestInfo &requestInfo ) const {
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetRoomsResponse{ 1, RoomManager::Instance().getRooms() } ), const_cast<MenuRequestHandler *>( this ) };
}

RequestResult MenuRequestHandler::getPlayersInRoom( const RequestInfo &requestInfo ) const {
	GetPlayersInRoomRequest getPlayersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest( requestInfo.m_buffer );
	std::vector<std::string> users;
	try {
		users = RoomManager::Instance().getRoom( getPlayersInRoomRequest.m_roomId ).getAllUsers();
	} catch ( const std::exception &e ) {
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ e.what() } ), const_cast<MenuRequestHandler *>( this ) };
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetPlayersInRoomResponse{ users } ), const_cast<MenuRequestHandler *>( this ) };
}

RequestResult MenuRequestHandler::getPersonalStats( const RequestInfo &requestInfo ) const {
	std::vector<std::string> statistics;
	uint32_t status = 1;
	try {
		statistics = StatisticsManager::Instance().getUserStatistics( this->m_user.getUsername() );
	} catch ( const std::exception & ) {
		statistics.clear();
		status = 0;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetPersonalStatsResponse{ status, statistics } ), const_cast<MenuRequestHandler *>( this ) };
}

RequestResult MenuRequestHandler::getHighScore( const RequestInfo &requestInfo ) const {
	std::vector<std::string> statistics;
	uint32_t status = 1;
	try {
		statistics = StatisticsManager::Instance().getHighScore();
	} catch ( const std::exception & ) {
		statistics.clear();
		status = 0;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetHighScoreResponse{ status, statistics } ), const_cast<MenuRequestHandler *>( this ) };
}

RequestResult MenuRequestHandler::joinRoom( const RequestInfo &requestInfo ) {
	JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest( requestInfo.m_buffer );
	try {
		RoomManager::Instance().getRoom( joinRoomRequest.m_roomId ).addUser( this->m_user );
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( JoinRoomResponse{ 1 } ), RequestHandlerFactory::Instance().createRoomMemberRequestHandler( RoomManager::Instance().getRoom( joinRoomRequest.m_roomId ).getMetadata(), this->m_user ) };
	} catch ( const std::exception & ) {
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( JoinRoomResponse{ 0 } ), this };
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Some error occured" } ), this };
}

RequestResult MenuRequestHandler::createRoom( const RequestInfo &requestInfo ) {
	CreateRoomRequest createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest( requestInfo.m_buffer );
	uint32_t status = 1;
	try {
		std::vector<RoomData> rooms = RoomManager::Instance().getRooms();
		RoomData metadata{ 
			rooms.empty() ? 0 : rooms.back().m_id + 1,
			createRoomRequest.m_roomName, createRoomRequest.m_maxUsers, 
			createRoomRequest.m_questionCount, 
			createRoomRequest.m_answerTimeout, 
			0 
		};
		RoomManager::Instance().createRoom( this->m_user, metadata );
		RoomManager::Instance().getRoom( metadata.m_id ).addUser( this->m_user );
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( CreateRoomResponse{ 1 } ), RequestHandlerFactory::Instance().createRoomAdminRequestHandler( metadata, this->m_user ) };
	} catch ( const std::exception & ) {
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse( CreateRoomResponse{ 0 } ), this };
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Some error occured" } ), this };
}
