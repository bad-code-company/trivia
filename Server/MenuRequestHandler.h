#ifndef MENU_REQUEST_HANDLER_H
#define MENU_REQUEST_HANDLER_H

#include "IRequestHandler.h"
#include "LoggedUser.h"

class MenuRequestHandler : public IRequestHandler {
private:
	/// fields
	LoggedUser m_user;
public:
	/**
	 * @brief Constructor
	 * @param user Thread's user
	 */
	MenuRequestHandler( const LoggedUser &user );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~MenuRequestHandler();

	/**
	 * @brief Returns is next request is relevant according to current
	 * @param requestInfo Next request info
	 * @return Is next request is relevant according to current
	 */
	virtual bool isRequestRelevant( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handels next request
	 * @param requestInfo Next request info
	 * @return Request result
	 */
	virtual RequestResult handleRequest( const RequestInfo &requestInfo );
	/**
	 * @brief Returns user's username
	 * @return User's username
	 */
	virtual std::string getUsername() const;
private:
	/**
	 * @brief Signs out from account
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult signout( const RequestInfo &requestInfo );
	/**
	 * @brief Handles 'get rooms' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getRooms( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handles 'get players in room' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getPlayersInRoom( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handles 'get personal stats' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getPersonalStats( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handles 'get high score' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getHighScore( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handles 'join room' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult joinRoom( const RequestInfo &requestInfo );
	/**
	 * @brief Handles 'create room' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult createRoom( const RequestInfo &requestInfo );
};

#endif // MENU_REQUEST_HANDLER_H
