#ifdef MONGO_DATABASE

#include "MongoDatabase.h"
#include "HttpsRequest.h"
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/collection.hpp>
#include <nlohmann/json.hpp>
#include <Base64.h>

#define MONGODB_URI_STRING	"mongodb://localhost:27017"
#define DATABASE_NAME		"trivia"
#define QUESTIONS_QUANTITY	10

mongocxx::instance	MongoDatabase::m_mongoInstance;
mongocxx::client	MongoDatabase::m_mongoClient( mongocxx::uri( MONGODB_URI_STRING ) );

MongoDatabase &MongoDatabase::Instance() {
	static MongoDatabase instance;
	return instance;
}

MongoDatabase::MongoDatabase() : IDatabase(), m_mongoDatabase( mongocxx::database( MongoDatabase::m_mongoClient[DATABASE_NAME] ) ) {
	this->addQuestionsToDatabase( QUESTIONS_QUANTITY );
}

MongoDatabase::~MongoDatabase() {}

void MongoDatabase::addQuestionsToDatabase( uint32_t quantity ) {
	// perform https request
	HttpsRequest httpsRequest( "opentdb.com" );
	std::string body = httpsRequest.get( "/api.php?amount=" + std::to_string( quantity ) + "&type=multiple&encode=base64" );
	if ( !body.empty() ) {
		// parse json
		nlohmann::json json = nlohmann::json::parse( body );
		// check response code
		if ( json.contains( "response_code" ) && json["response_code"].get<uint32_t>() == 0 ) {
			for ( uint32_t i = 0; i < quantity; ++i ) {
				// decode base64
				std::string question, correctAnswer, secondAnswer, thirdAnswer, fourthAnswer;
				macaron::Base64::Decode( json["results"][i]["question"].get<std::string>(), question );
				macaron::Base64::Decode( json["results"][i]["correct_answer"].get<std::string>(), correctAnswer );
				macaron::Base64::Decode( json["results"][i]["incorrect_answers"][0].get<std::string>(), secondAnswer );
				macaron::Base64::Decode( json["results"][i]["incorrect_answers"][1].get<std::string>(), thirdAnswer );
				macaron::Base64::Decode( json["results"][i]["incorrect_answers"][2].get<std::string>(), fourthAnswer );
				// try insert (cauz question may appear into a database)
				try {
					auto insert =
						bsoncxx::builder::stream::document()
							<< "_id"				<< question
							<< "correct_answer"		<< correctAnswer
							<< "incorrect_answers"	<< bsoncxx::builder::stream::open_document
								<< "0"					<< secondAnswer
								<< "1"					<< thirdAnswer
								<< "2"					<< fourthAnswer
							<< bsoncxx::builder::stream::close_document
						<< bsoncxx::builder::stream::finalize;
					this->m_mongoDatabase["questions"].insert_one( insert.view() );
				} catch ( ... ) {}
			}
		}
	}
}

bool MongoDatabase::doesUserExist( const std::string &username ) const {
	auto user = this->m_mongoDatabase["users"].find_one( bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize );
	return user.has_value();
}

bool MongoDatabase::doesPasswordMatch( const std::string &username, const std::string &password ) const {
	auto user = this->m_mongoDatabase["users"].find_one( bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize );
	// if user found
	if ( user.has_value() ) {
		// parse json
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( *user ) );
		// check password
		return json["password"].get<std::string>() == password;
	}
	throw std::exception( "User does not exists" );
}

void MongoDatabase::addNewUser( const std::string &username, const std::string &password, const std::string &email ) {
	// build insert document
	auto insert =
		bsoncxx::builder::stream::document()
			<< "_id"		<< username
			<< "password"	<< password
			<< "email"		<< email
		<< bsoncxx::builder::stream::finalize;
	this->m_mongoDatabase["users"].insert_one( insert.view() );
}

std::vector<Question> MongoDatabase::getQuestions( const int quantity ) const {
	std::vector<Question> questions;
	// get 5 random questions
	auto questionsCursor = this->m_mongoDatabase["questions"].aggregate( mongocxx::pipeline().sample( quantity ) );
	// insert questions to vector
	for ( auto &&question : questionsCursor ) {
		// parse json
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( question ) );
		// push to vector
		questions.push_back(
			Question( 
				json["_id"].get<std::string>(), 
				json["correct_answer"].get<std::string>(), 
				std::vector<std::string>{ 
					json["correct_answer"].get<std::string>(), 
					json["incorrect_answers"]["0"].get<std::string>(),
					json["incorrect_answers"]["1"].get<std::string>(),
					json["incorrect_answers"]["2"].get<std::string>() 
				}
			) 
		);
	}
	return questions;
}

float MongoDatabase::getPlayerAverageAnswerTime( const std::string &username ) const {
	auto userStatistics = this->m_mongoDatabase["statistics"].find_one( bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize );
	if ( userStatistics ) {
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( *userStatistics ) );
		return json["answer_time"].get<float>();
	}
	throw std::exception( "No statistics for current user" );
}

int MongoDatabase::getNumOfCorrectAnswers( const std::string &username ) const {
	auto userStatistics = this->m_mongoDatabase["statistics"].find_one( bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize );
	if ( userStatistics ) {
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( *userStatistics ) );
		return json["correct_answers"].get<int>();
	}
	throw std::exception( "No statistics for current user" );
}

int MongoDatabase::getNumOfTotalAnswers( const std::string &username ) const {
	auto userStatistics = this->m_mongoDatabase["statistics"].find_one( bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize );
	if ( userStatistics ) {
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( *userStatistics ) );
		return json["total_answers"].get<int>();
	}
	throw std::exception( "No statistics for current user" );
}

int MongoDatabase::getNumOfPlayerGames( const std::string &username ) const {
	auto userStatistics = this->m_mongoDatabase["statistics"].find_one( bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize );
	if ( userStatistics ) {
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( *userStatistics ) );
		return json["games_played"].get<int>();
	}
	throw std::exception( "No statistics for current user" );
}

std::vector<std::string> MongoDatabase::getBestPlayers() const {
	std::vector<std::string> bestPlayers;
	mongocxx::options::find findOptions;
	auto order = bsoncxx::builder::stream::document() << "correct_answers" << -1 << bsoncxx::builder::stream::finalize;
	findOptions.sort( order.view() );
	findOptions.limit( 5 );
	auto statisticsCursor = this->m_mongoDatabase["statistics"].find( {}, findOptions );
	for ( auto &&statistics : statisticsCursor ) {
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( statistics ) );
		bestPlayers.push_back( json["_id"].get<std::string>() + ": " + std::to_string( json["correct_answers"].get<int>() ) );
	}
	return bestPlayers;
}

void MongoDatabase::addNewGame() {
	mongocxx::options::find findOptions;
	auto order = bsoncxx::builder::stream::document() << "_id" << -1 << bsoncxx::builder::stream::finalize;
	findOptions.sort( order.view() );
	auto lastGame = this->m_mongoDatabase["games"].find_one( {}, findOptions );
	if ( lastGame ) {
		// if game exists, insert new game with id + 1
		nlohmann::json json = nlohmann::json::parse( bsoncxx::to_json( *lastGame ) );
		this->m_mongoDatabase["games"].insert_one( bsoncxx::builder::stream::document() << "_id" << json["_id"].get<int>() + 1 << bsoncxx::builder::stream::finalize );
	} else {
		// if does not exist, insert game with id 0
		this->m_mongoDatabase["games"].insert_one( bsoncxx::builder::stream::document() << "_id" << 0 << bsoncxx::builder::stream::finalize );
	}
}

void MongoDatabase::updateUserStatistics( const std::string &username, const int gamesPlayed, const int totalAnswers, const int correctAnswers, const float answerTime ) {
	try {
		// try to insert
		auto insert =
			bsoncxx::builder::stream::document()
				<< "_id" << username
				<< "games_played" << gamesPlayed
				<< "total_answers" << totalAnswers
				<< "correct_answers" << correctAnswers
				<< "answer_time" << answerTime
			<< bsoncxx::builder::stream::finalize;
		this->m_mongoDatabase["statistics"].insert_one( insert.view() );
	} catch ( ... ) {
		// if insert does not work, just update
		auto filter = bsoncxx::builder::stream::document() << "_id" << username << bsoncxx::builder::stream::finalize;
		auto update =
			bsoncxx::builder::stream::document()
				<< "$set" << bsoncxx::builder::stream::open_document
					<< "games_played" 		<< gamesPlayed
					<< "total_answers" 		<< totalAnswers
					<< "correct_answers"	<< correctAnswers
					<< "answer_time" 		<< answerTime
				<< bsoncxx::builder::stream::close_document
			<< bsoncxx::builder::stream::finalize;
		this->m_mongoDatabase["statistics"].update_one( filter.view(), update.view() );
	}
}

#endif // MONGO_DATABASE