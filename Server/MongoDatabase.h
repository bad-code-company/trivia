#ifdef MONGO_DATABASE

#ifndef MONGO_DATABASE_H
#define MONGO_DATABASE_H

/*
Tables' info:

	- users:
	{
		_id				: string
		password		: string
		email			: string
	}

	- questions:
	{
		_id (question)	: string
		correct_answer	: string
		incorrect_answers:
		{
			0			: string
			1			: string
			2			: string
		}
	}

	- games
	{
		_id (game_id)	: int
	}

	- statistics
	{
		_id ("nikita")	: string
		games_played	: int
		total_answers	: int
		correct_answers	: int
		answer_time		: float
	}
*/

#include "IDatabase.h"
#include <mongocxx/instance.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/database.hpp>

class MongoDatabase : public IDatabase {
private:
	/// values that must be initialized only once
	static mongocxx::instance	m_mongoInstance;
	static mongocxx::client		m_mongoClient;

	/// fields
	mongocxx::database			m_mongoDatabase;

	/**
	 * @brief Default constructor
	 */
	MongoDatabase();
public:
	/**
	 * @brief Returns mongodb database instance
	 * @return Mongodb database instance
	 */
	static MongoDatabase &Instance();
	/// Delete copy constructor
	MongoDatabase( const MongoDatabase & ) = delete;
	/// delete = operator
	void operator=( const MongoDatabase & ) = delete;
	/**
	 * @brief Default virtual destructor
	 */
	virtual ~MongoDatabase();
	/**
	 * @brief Checks does user exist
	 * @param username User name to find
	 * @return Does user exist
	 */
	virtual bool doesUserExist( const std::string &username ) const;
	/**
	 * @brief Checks does password match to user (according to user name)
	 * @param username User's name
	 * @param password Password to check
	 * @return Does password match to user
	 */
	virtual bool doesPasswordMatch( const std::string &username, const std::string &password ) const;
	/**
	 * @brief Adds new user to a database
	 * @param username User's name to add
	 * @param password User's password
	 * @param email User's email
	 */
	virtual void addNewUser( const std::string &username, const std::string &password, const std::string &email );

	/**
	 * @brief Returns 'quantity' questions from the database
	 * @param quantity Question quantity
	 * @return 'Quantity' questions from the database
	 */
	virtual std::vector<Question> getQuestions( const int quantity ) const;
	/**
	 * @brief Returns user's average answer time
	 * @param username User's username
	 * @return User's average answer time
	 */
	virtual float getPlayerAverageAnswerTime( const std::string &username ) const;
	/**
	 * @brief Returns number of correct answers of the user
	 * @param username User's username
	 * @return Number of correct answers of the user
	 */
	virtual int getNumOfCorrectAnswers( const std::string &username ) const;
	/**
	 * @brief Returns number of answers of the user
	 * @param username User's username
	 * @return Number of answers of the user
	 */
	virtual int getNumOfTotalAnswers( const std::string &username ) const;
	/**
	 * @brief Returns number of games of the user
	 * @param username User's username
	 * @return Number of games of the user
	 */
	virtual int getNumOfPlayerGames( const std::string &username ) const;
	/**
	 * @brief Returns vector of 5 best players
	 * @return Vector of 5 best players
	 */
	virtual std::vector<std::string> getBestPlayers() const;

	/**
	 * @brief Adds new game to database
	 */
	virtual void addNewGame();
	/**
	 * @brief Updates user statistics
	 * @param username User to update stats
	 * @param gamesPlayed New games played value
	 * @param totalAnswers New total answers value
	 * @param correctAnswers New correct answers value
	 * @param answerTime New answer time value
	 */
	virtual void updateUserStatistics( const std::string &username, const int gamesPlayed, const int totalAnswers, const int correctAnswers, const float answerTime );
private:
	/**
	 * @brief Adds new questions to db
	 * @param quantity Number of questions to add
	 */
	virtual void addQuestionsToDatabase( uint32_t quantity );
};

#endif // MONGO_DATABASE_H

#endif // MONGO_DATABASE