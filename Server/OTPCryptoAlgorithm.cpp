#include "OTPCryptoAlgorithm.h"
#include <random>

OTPCryptoAlgorithm &OTPCryptoAlgorithm::Instance() {
	static OTPCryptoAlgorithm instance;
	return instance;
}

std::vector<uint8_t> OTPCryptoAlgorithm::encrypt( const std::vector<uint8_t> &buffer ) {
	// random engine
	std::mt19937 generator( std::random_device{}( ) ); // generator
	std::uniform_int_distribution<> range( 0, 255 ); // range
	// encrypt and build the key
	std::vector<uint8_t> crypted, key;
	for ( auto &&byte : buffer ) {
		key.push_back( range( generator ) );
		crypted.push_back( ( byte + key.back() ) % 256 );
	}
	// join crypted vector to the key vector
	for ( auto &&byte : crypted ) key.push_back( byte );
	return key;
}

std::vector<uint8_t> OTPCryptoAlgorithm::decrypt( const std::vector<uint8_t> &buffer ) {
	std::vector<uint8_t> crypted, key, decrypted;
	size_t i = 0;
	// separate between crypted and the key
	for ( ; i < buffer.size() / 2; ++i ) key.push_back( buffer[i] );
	for ( ; i < buffer.size(); ++i ) crypted.push_back( buffer[i] );
	// decrypt
	for ( i = 0; i < crypted.size(); ++i ) {
		decrypted.push_back( crypted[i] - key[i] );
	}
	return decrypted;
}
