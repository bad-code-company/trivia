#ifndef OTPCRYPTO_ALGORITHM_H
#define OTPCRYPTO_ALGORITHM_H

#include "ICryptoAlgorithm.h"

class OTPCryptoAlgorithm : public ICryptoAlgorithm {
private:
	/**
	 * @brief Default constructor
	 */
	OTPCryptoAlgorithm() = default;
public:
	/**
	 * @brief Returns OTP crypto algorithm database instance
	 * @return OTP crypto algorithm database instance
	 */
	static OTPCryptoAlgorithm &Instance();
	/// Delete copy constructor
	OTPCryptoAlgorithm( const OTPCryptoAlgorithm & ) = delete;
	/// Delete = operator
	void operator=( const OTPCryptoAlgorithm & ) = delete;
	/**
	 * @brief Default virtual destructor
	 */
	virtual ~OTPCryptoAlgorithm() = default;

	/**
	 * @brief Encrypts the buffer and returns it
	 * @param buffer Buffer to encrypt
	 * @return Encrypted buffer
	 */
	virtual std::vector<uint8_t> encrypt( const std::vector<uint8_t> &buffer );
	/**
	 * @brief Decrypts the buffer and returns it
	 * @param buffer Buffer to decrypt
	 * @return Decrypted buffer
	 */
	virtual std::vector<uint8_t> decrypt( const std::vector<uint8_t> &buffer );
};

#endif // OTPCRYPTO_ALGORITHM_H
