#include "Question.h"
#include <algorithm>
#include <random>

Question::Question( const std::string &question, const std::string &correctAnswer, const std::vector<std::string> &possibleAnswers ) : m_question( question ), m_correctAnswer( correctAnswer ), m_possibleAnswers( possibleAnswers ) {
	std::shuffle( this->m_possibleAnswers.begin(), this->m_possibleAnswers.end(), std::mt19937( std::random_device{}() ) );
}

Question::Question( const Question &other ) : m_question( other.m_question ), m_correctAnswer( other.m_correctAnswer ), m_possibleAnswers( other.m_possibleAnswers ) {}

Question::~Question() {}

std::string Question::getQuestion() const {
	return this->m_question;
}

std::string Question::getCorrectAnswer() const {
	return this->m_correctAnswer;
}

std::map<uint32_t, std::string> Question::getPossibleAnswers() const {
	std::map<uint32_t, std::string> possibleAnswers{
		std::make_pair( static_cast<uint32_t>( 0 ), this->m_possibleAnswers[0] ),
		std::make_pair( static_cast<uint32_t>( 1 ), this->m_possibleAnswers[1] ),
		std::make_pair( static_cast<uint32_t>( 2 ), this->m_possibleAnswers[2] ),
		std::make_pair( static_cast<uint32_t>( 3 ), this->m_possibleAnswers[3] )
	};
	return possibleAnswers;
}