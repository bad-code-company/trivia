#ifndef QUESTION_H
#define QUESTION_H

#include <string>
#include <map>
#include <vector>

class Question {
private:
	/// fields
	std::string m_question;
	std::string m_correctAnswer;
	std::vector<std::string> m_possibleAnswers;
public:
	/**
	 * @brief Constructor
	 * @param question Queston
	 * @param correctAnswer Correct answer
	 * @param invalidAnswers Invalid answers vector
	 */
	Question( const std::string &question, const std::string &correctAnswer, const std::vector<std::string> &possibleAnswers );
	/**
	 * @brief Copy constructor
	 * @param other Question to copy
	 */
	Question( const Question &other );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~Question();

	/**
	 * @brief Returns question
	 * @return Question
	 */
	std::string getQuestion() const;
	/**
	 * @brief Returns correct answer
	 * @return Correct answer
	 */
	std::string getCorrectAnswer() const;
	/**
	 * @brief Returns invalid answers
	 * @return Invalid answers
	 */
	std::map<uint32_t, std::string> getPossibleAnswers() const;
};

#endif // QUESTION_H
