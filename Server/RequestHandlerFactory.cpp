#include "RequestHandlerFactory.h"
#include "Server.h"

RequestHandlerFactory &RequestHandlerFactory::Instance() {
	static RequestHandlerFactory instance;
	return instance;
}

RequestHandlerFactory::RequestHandlerFactory() {}

RequestHandlerFactory::~RequestHandlerFactory() {}

LoginRequestHandler *RequestHandlerFactory::createLoginRequestHandler() {
	return new LoginRequestHandler();
}

MenuRequestHandler *RequestHandlerFactory::createMenuRequestHandler( const LoggedUser &user ) {
	return new MenuRequestHandler( user );
}

RoomAdminRequestHandler *RequestHandlerFactory::createRoomAdminRequestHandler( const RoomData &metadata, const LoggedUser &user ) {
	return new RoomAdminRequestHandler( metadata, user );
}

RoomMemberRequestHandler *RequestHandlerFactory::createRoomMemberRequestHandler( const RoomData &metadata, const LoggedUser &user ) {
	return new RoomMemberRequestHandler( metadata, user );
}

GameRequestHandler *RequestHandlerFactory::createGameRequestHandler( Game &game, const LoggedUser &user ) {
	return new GameRequestHandler( game, user );
}
