#ifndef REQUEST_HANDLER_FACTORY_H
#define REQUEST_HANDLER_FACTORY_H

#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"

class RequestHandlerFactory {
private:
	/**
	 * @brief Contructor
	 */
	RequestHandlerFactory();
public:
	/**
	 * @brief Returns request handler factory
	 * @return Request handler factory instance
	 */
	static RequestHandlerFactory &Instance();
	/// Delete copy constructor
	RequestHandlerFactory( const RequestHandlerFactory & ) = delete;
	/// delete = operator
	void operator=( const RequestHandlerFactory & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~RequestHandlerFactory();

	/**
	 * @brief Creates login request handler
	 * @return New login request handler
	 */
	LoginRequestHandler *createLoginRequestHandler();
	/**
	 * @brief Creates menu request handler
	 * @param user User to create menu request handler
	 * @return New menu request handler
	 */
	MenuRequestHandler *createMenuRequestHandler( const LoggedUser &user );
	/**
	 * @brief Creates room admin request handler
	 * @param metadata Room metadata
	 * @param user User to create room admin request handler
	 * @return New room admin request handler
	 */
	RoomAdminRequestHandler *createRoomAdminRequestHandler( const RoomData &metadata, const LoggedUser &user );
	/**
	 * @brief Creates room member request handler
	 * @param metadata Room metadata
	 * @param user User to create room member request handler
	 * @return New room member request handler
	 */
	RoomMemberRequestHandler *createRoomMemberRequestHandler( const RoomData &metadata, const LoggedUser &user );
	/**
	 * @brief Creates game request handler
	 * @param game Game
	 * @param user User to create game request handler
	 * @return New game request handler
	 */
	GameRequestHandler *createGameRequestHandler( Game &game, const LoggedUser &user );
};

#endif // REQUEST_HANDLER_FACTORY_H
