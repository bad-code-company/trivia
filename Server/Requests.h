#ifndef REQUESTS_H
#define REQUESTS_H

#include <cstdint>
#include <ctime>
#include <vector>
#include <string>

class IRequestHandler;

enum class RequestId : uint8_t {
	LOGIN_ID				= 10,
	SIGNUP_ID				= 20,
	LOGOUT_ID				= 30, 
	GET_ROOMS_ID			= 40,
	GET_PLAYERS_IN_ROOM_ID	= 50,
	GET_HIGH_SCORE_ID		= 60,
	GET_PERSONAL_STATS_ID	= 70,
	JOIN_ROOM_ID			= 80,
	CREATE_ROOM_ID			= 90,
	CLOSE_ROOM_ID			= 100,
	START_GAME_ID			= 110,
	GET_ROOM_STATE_ID		= 120,
	LEAVE_ROOM_ID			= 130,
	LEAVE_GAME_ID			= 140,
	GET_QUESTION_ID			= 150,
	SUBMIT_ANSWER_ID		= 160,
	GET_GAME_RESULT			= 170
};

struct RequestInfo {
	::time_t m_recvTime;
	RequestId m_code;
	std::vector<uint8_t> m_buffer;
};

struct RequestResult {
	std::vector<uint8_t> m_buffer;
	IRequestHandler *m_newHandler = nullptr;
};

struct LoginRequest {
	std::string m_username;
	std::string m_password;
};

struct SignupRequest {
	std::string m_username;
	std::string m_password;
	std::string m_email;
};

struct GetPlayersInRoomRequest {
	uint32_t m_roomId;
};

struct JoinRoomRequest {
	uint32_t m_roomId;
};

struct CreateRoomRequest {
	std::string m_roomName;
	uint32_t m_maxUsers;
	uint32_t m_questionCount;
	uint32_t m_answerTimeout;
};

struct SubmitAnswerRequest {
	uint32_t m_answerId;
};

#endif // REQUESTS_H
