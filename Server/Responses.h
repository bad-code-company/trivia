#ifndef RESPONSES_H
#define RESPONSES_H

#include <string>
#include <vector>
#include <map>

struct RoomData;

/**
 * An enum of response codes.
 */
enum class ResponseId : uint8_t {
	ERROR_ID				= 1,
	LOGIN_ID				= 11,
	SIGNUP_ID				= 21,
	LOGOUT_ID				= 31,
	GET_ROOMS_ID			= 41,
	GET_PLAYERS_IN_ROOM_ID	= 51,
	GET_HIGH_SCORE_ID		= 61,
	GET_PERSONAL_STATS_ID	= 71,
	JOIN_ROOM_ID			= 81,
	CREATE_ROOM_ID			= 91,
	CLOSE_ROOM_ID			= 101,
	START_GAME_ID			= 111,
	GET_ROOM_STATE_ID		= 121,
	LEAVE_ROOM_ID			= 131,
	LEAVE_GAME_ID			= 141,
	GET_QUESTION_ID			= 151,
	SUBMIT_ANSWER_ID		= 161,
	GET_GAME_RESULT_ID		= 171
};

/**
 * A response struct indicating an error.
 */
struct ErrorResponse {
	std::string m_message;
};

/**
 * A response struct to a login request.
 */
struct LoginResponse {
	uint32_t m_status;
};

/**
 * A response struct to a signup request.
 */
struct SignupResponse {
	uint32_t m_status;
};

struct LogoutResponse {
	uint32_t m_status;
};

struct GetRoomsResponse {
	uint32_t				m_status;
	std::vector<RoomData>	m_rooms;
};

struct GetPlayersInRoomResponse {
	std::vector<std::string> m_players;
};

struct GetHighScoreResponse {
	uint32_t					m_status;
	std::vector<std::string>	m_statistics;
};

struct GetPersonalStatsResponse {
	uint32_t					m_status;
	std::vector<std::string>	m_statistics;
};

struct JoinRoomResponse {
	uint32_t m_status;
};

struct CreateRoomResponse {
	uint32_t m_status;
};

struct CloseRoomResponse {
	uint32_t m_status;
};

struct StartGameResponse {
	uint32_t m_status;
};

struct GetRoomStateResponse {
	uint32_t					m_status;
	bool						m_hasGameBegun;
	std::vector<std::string>	m_players;
	uint32_t					m_questionCount;
	uint32_t					m_answerTimeout;
};

struct LeaveRoomResponse {
	uint32_t m_status;
};

struct LeaveGameResponse {
	uint32_t m_status;
};

struct GetQuestionResponse {
	uint32_t						m_status;
	std::string						m_question;
	std::map<uint32_t, std::string> m_answers;
};

struct SubmitAnswerResponse {
	uint32_t m_status;
	uint32_t m_correctAnswerId;
};

struct PlayerResults {
	std::string m_username;
	uint32_t	m_correctAnswerCount;
	uint32_t	m_wrongAnswerCount;
	float		m_averageAnswerTime;
};

struct GetGameResultsResponse {
	uint32_t					m_status;
	std::vector<PlayerResults>	m_results;
};

#endif // RESPONSES_H
