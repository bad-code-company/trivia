#include "Room.h"

std::mutex Room::m_removeUserMtx;

Room::Room( const RoomData &metadata ) : m_metadata( metadata ) {}

Room::~Room() {}

void Room::addUser( const LoggedUser &user ) {
	if ( this->m_users.size() == this->m_metadata.m_maxPlayers ) throw std::exception( "Max count of players is already reached" );
	if ( std::find( this->m_users.begin(), this->m_users.end(), user ) != this->m_users.end() ) throw std::exception( "User is already into room" );
	this->m_users.push_back( user );
}

void Room::removeUser( const LoggedUser &user ) {
	std::unique_lock<std::mutex> removeUserLock( Room::m_removeUserMtx );
	if ( this->m_users.empty() || std::find( this->m_users.begin(), this->m_users.end(), user ) == this->m_users.end() ) throw std::exception( "User is not into room" );
	this->m_users.erase( std::find( this->m_users.begin(), this->m_users.end(), user ) );
	removeUserLock.unlock();
}

std::vector<std::string> Room::getAllUsers() const {
	std::vector<std::string> allUsers;
	for ( auto &&user : this->m_users ) allUsers.push_back( user.getUsername() );
	return allUsers;
}

RoomData Room::getMetadata() const {
	return this->m_metadata;
}