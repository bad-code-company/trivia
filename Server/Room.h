#ifndef ROOM_H
#define ROOM_H

#include <cstdint>
#include <string>
#include <vector>
#include <mutex>
#include "LoggedUser.h"

struct RoomData {
	uint32_t m_id;
	std::string m_name;
	uint32_t m_maxPlayers;
	uint32_t m_numOfQuestionsInGame;
	uint32_t m_timePerQuestion;
	uint32_t m_isActive;
};

class Room {
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users; /// active users

	static std::mutex m_removeUserMtx;
public:
	/**
	 * @brief Constructor
	 */
	Room( const RoomData &metadata );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~Room();

	/**
	 * @brief Adds an active user
	 * @param user New user
	 */
	void addUser( const LoggedUser &user );
	/**
	 * @brief Removes user
	 * @param user User to remove
	 */
	void removeUser( const LoggedUser &user );
	/**
	 * @brief Returns list of usernames of active users
	 * @return List of usernames of active users
	 */
	std::vector<std::string> getAllUsers() const;
	/**
	 * @brief Returns room's metadata
	 * @return Room's metadata
	 */
	RoomData getMetadata() const;
};

#endif // ROOM_H
