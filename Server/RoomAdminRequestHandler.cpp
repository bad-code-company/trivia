#include "RoomAdminRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "Communicator.h"
#include "GameManager.h"

RoomAdminRequestHandler::RoomAdminRequestHandler( const RoomData &metadata, const LoggedUser &user ) : IRequestHandler(), m_room( RoomManager::Instance().getRoom( metadata.m_id ) ), m_user( user ) {}

RoomAdminRequestHandler::~RoomAdminRequestHandler() {}

bool RoomAdminRequestHandler::isRequestRelevant( const RequestInfo &requestInfo ) const {
	return requestInfo.m_code == RequestId::CLOSE_ROOM_ID || requestInfo.m_code == RequestId::START_GAME_ID || requestInfo.m_code == RequestId::GET_ROOM_STATE_ID;
}

RequestResult RoomAdminRequestHandler::handleRequest( const RequestInfo &requestInfo ) {
	switch ( requestInfo.m_code ) {
		case RequestId::CLOSE_ROOM_ID:
			return this->closeRoom( requestInfo );
			break;
		case RequestId::START_GAME_ID:
			return this->startGame( requestInfo );
			break;
		case RequestId::GET_ROOM_STATE_ID:
			return this->getRoomState( requestInfo );
			break;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Invalid request" } ), this };
}

std::string RoomAdminRequestHandler::getUsername() const {
	return this->m_user.getUsername();
}

RequestResult RoomAdminRequestHandler::closeRoom( const RequestInfo &requestInfo ) {
	uint32_t status = 1;
	try {
		for ( auto &&username : this->m_room.getAllUsers() ) {
			if ( username != this->m_user.getUsername() ) {
				try {
					// get user socket and request handler
					std::pair<const SOCKET, IRequestHandler *> &user = Communicator::Instance().getUser( username );
					if ( user.first ) {
						RequestResult requestResult = user.second->handleRequest( RequestInfo{ ::time( nullptr ), RequestId::LEAVE_ROOM_ID, std::vector<uint8_t>() } );
						delete user.second;
						user.second = requestResult.m_newHandler;
						std::vector<uint8_t> leaveRoomResponse = JsonResponsePacketSerializer::serializeResponse( LeaveRoomResponse{ 1 } );
						try {
							Communicator::Instance().sendMessage( user.first, leaveRoomResponse );
						} catch ( ... ) {}
						if ( this->m_room.getAllUsers().empty() ) break;
					}
				} catch ( ... ) {}
			}
		}
		RoomManager::Instance().deleteRoom( this->m_room.getMetadata().m_id );
	} catch ( std::exception & ) {
		status = 0;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( CloseRoomResponse{ status } ),  RequestHandlerFactory::Instance().createMenuRequestHandler( this->m_user ) };
}

RequestResult RoomAdminRequestHandler::startGame( const RequestInfo &requestInfo ) {
	uint32_t status = 1;
	auto &game = GameManager::Instance().createGame( this->m_room );
	try {
		for ( auto &&username : this->m_room.getAllUsers() ) {
			if ( username != this->m_user.getUsername() ) {
				try {
					// get user socket and request handler
					std::pair<const SOCKET, IRequestHandler *> &user = Communicator::Instance().getUser( username );
					if ( user.first ) {
						RequestResult requestResult = user.second->handleRequest( RequestInfo{ ::time( nullptr ), RequestId::START_GAME_ID, std::vector<uint8_t>() } );
						delete user.second;
						user.second = requestResult.m_newHandler;
						std::vector<uint8_t> startGameResponse = JsonResponsePacketSerializer::serializeResponse( StartGameResponse{ 1 } );
						try {
							Communicator::Instance().sendMessage( user.first, startGameResponse );
						} catch ( ... ) {}
						if ( this->m_room.getAllUsers().empty() ) break;
					}
				} catch ( ... ) {}
			}
		}
	} catch ( std::exception & ) {
		status = 0;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( StartGameResponse{ status } ), RequestHandlerFactory::Instance().createGameRequestHandler( game, this->m_user ) };
}

RequestResult RoomAdminRequestHandler::getRoomState( const RequestInfo &requestInfo ) {
	RoomData metadata = this->m_room.getMetadata();
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetRoomStateResponse{ 1, static_cast<bool>( metadata.m_isActive ), this->m_room.getAllUsers(), metadata.m_numOfQuestionsInGame, metadata.m_timePerQuestion  } ), this };
}
