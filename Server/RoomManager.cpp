#include "RoomManager.h"

RoomManager &RoomManager::Instance() {
	static RoomManager instance;
	return instance;
}

RoomManager::RoomManager() {}

RoomManager::~RoomManager() {}

void RoomManager::createRoom( const LoggedUser &creator, const RoomData &metadata ) {
	if ( this->m_rooms.find( metadata.m_id ) != this->m_rooms.end() ) throw std::exception( "Room with this ID already exists" );
	this->m_rooms.insert( std::make_pair( metadata.m_id, Room( metadata ) ) );
}

void RoomManager::deleteRoom( uint32_t id ) {
	if ( this->m_rooms.empty() || this->m_rooms.find( id ) == this->m_rooms.end() ) throw std::exception( "Room with this ID does not exist" );
	this->m_rooms.erase( id );
}

uint32_t RoomManager::getRoomState( uint32_t id ) const {
	if( this->m_rooms.empty() || this->m_rooms.find( id ) == this->m_rooms.end() ) throw std::exception( "Room with this ID does not exist" );
	return this->m_rooms.at( id ).getMetadata().m_isActive;
}

std::vector<RoomData> RoomManager::getRooms() const {
	std::vector<RoomData> rooms;
	for ( auto &&room : this->m_rooms ) {
		rooms.push_back( room.second.getMetadata() );
	}
	return rooms;
}

Room &RoomManager::getRoom( uint32_t id ) {
	if ( this->m_rooms.empty() || this->m_rooms.find( id ) == this->m_rooms.end() ) throw std::exception( "Room with this ID does not exist" );
	return this->m_rooms.at( id );
}

std::map<uint32_t, Room> &RoomManager::getRoomsRefs() {
	return this->m_rooms;
}
