#ifndef ROOM_MANAGER_H
#define ROOM_MANAGER_H

#include <cstdint>
#include <map>
#include <vector>
#include <cstdint>
#include "Room.h"

class RoomManager {
private:
	/// fields
	std::map<uint32_t, Room> m_rooms;

	/**
	 * @brief Constructor
	 */
	RoomManager();
public:
	/**
	 * @brief Returns room manager factory
	 * @return Room manager instance
	 */
	static RoomManager &Instance();
	/// Delete copy constructor
	RoomManager( const RoomManager & ) = delete;
	/// delete = operator
	void operator=( const RoomManager & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~RoomManager();

	/**
	 * @brief Creates room and puts it into a rooms' map
	 * @param creator Room's creator
	 * @param metadata Room's metadata
	 */
	void createRoom( const LoggedUser &creator, const RoomData &metadata );
	/**
	 * @brief Deletes room
	 * @param id Room id
	 */
	void deleteRoom( uint32_t id );
	/**
	 * @brief Returns room's state
	 * @param id Room's id
	 * @return Room's state
	 */
	uint32_t getRoomState( uint32_t id ) const;
	/**
	 * @brief Returns all rooms' metadata
	 * @return All rooms' metadata
	 */
	std::vector<RoomData> getRooms() const;
	/**
	 * @brief Returns room by id
	 * @param id Room id
	 * @return
	 */
	Room &getRoom( uint32_t id );
	/**
	 * @brief Returns rooms' map ref
	 * @return Rooms' map ref
	 */
	std::map<uint32_t, Room> &getRoomsRefs();
};

#endif // ROOM_MANAGER_H
