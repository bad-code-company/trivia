#include "RoomAdminRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "GameManager.h"

RoomMemberRequestHandler::RoomMemberRequestHandler( const RoomData &metadata, const LoggedUser &user ) : IRequestHandler(), m_room( RoomManager::Instance().getRoom( metadata.m_id ) ), m_user( user ) {}

RoomMemberRequestHandler::~RoomMemberRequestHandler() {}

bool RoomMemberRequestHandler::isRequestRelevant( const RequestInfo &requestInfo ) const {
	return requestInfo.m_code == RequestId::LEAVE_ROOM_ID || requestInfo.m_code == RequestId::START_GAME_ID || requestInfo.m_code == RequestId::GET_ROOM_STATE_ID;
}

RequestResult RoomMemberRequestHandler::handleRequest( const RequestInfo &requestInfo ) {
		switch ( requestInfo.m_code ) {
		case RequestId::LEAVE_ROOM_ID:
			return this->leaveRoom( requestInfo );
			break;
		case RequestId::START_GAME_ID:
			return this->startGame( requestInfo );
			break;
		case RequestId::GET_ROOM_STATE_ID:
			return this->getRoomState( requestInfo );
			break;
	}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( ErrorResponse{ "Invalid request" } ), this };
}

std::string RoomMemberRequestHandler::getUsername() const {
	return this->m_user.getUsername();
}

RequestResult RoomMemberRequestHandler::leaveRoom( const RequestInfo &requestInfo ) {
	try {
		this->m_room.removeUser( this->m_user );
	} catch ( std::exception & ) {}
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( LeaveRoomResponse{ 1 } ),  RequestHandlerFactory::Instance().createMenuRequestHandler( this->m_user ) };
}

RequestResult RoomMemberRequestHandler::startGame( const RequestInfo &requestInfo ) {
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( StartGameResponse{ 1 } ), RequestHandlerFactory::Instance().createGameRequestHandler( GameManager::Instance().getGame( this->m_room.getMetadata().m_id ), this->m_user ) };
}

RequestResult RoomMemberRequestHandler::getRoomState( const RequestInfo &requestInfo ) {
	RoomData metadata = this->m_room.getMetadata();
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse( GetRoomStateResponse{ 1, static_cast<bool>( metadata.m_isActive ), this->m_room.getAllUsers(), metadata.m_numOfQuestionsInGame, metadata.m_timePerQuestion  } ), this };
}
