#ifndef ROOM_MEMBER_REQUEST_HANDLER_H
#define ROOM_MEMBER_REQUEST_HANDLER_H

#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "Room.h"

class RoomMemberRequestHandler : public IRequestHandler {
private:
	/// fields
	Room &m_room;
	LoggedUser m_user;
public:
	/**
	 * @brief Constructor
	 * @param metadata Room metadata
	 * @param user Handler's user
	 */
	RoomMemberRequestHandler( const RoomData &metadata, const LoggedUser &user );
	/**
	 * @brief Virtual destructor
	 */
	virtual ~RoomMemberRequestHandler();

	/**
	 * @brief Returns is next request is relevant according to current
	 * @param requestInfo Next request info
	 * @return Is next request is relevant according to current
	 */
	virtual bool isRequestRelevant( const RequestInfo &requestInfo ) const;
	/**
	 * @brief Handels next request
	 * @param requestInfo Next request info
	 * @return Request result
	 */
	virtual RequestResult handleRequest( const RequestInfo &requestInfo );
	/**
	 * @brief Returns user's username
	 * @return User's username
	 */
	virtual std::string getUsername() const;
private:
	/**
	 * @brief Handles 'leave room' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult leaveRoom( const RequestInfo &requestInfo );
	/**
	 * @brief Handles 'start room' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult startGame( const RequestInfo &requestInfo );
	/**
	 * @brief Handles 'get room state' request
	 * @param requestInfo Request info
	 * @return Request result
	 */
	RequestResult getRoomState( const RequestInfo &requestInfo );
};

#endif // ROOM_MEMBER_REQUEST_HANDLER_H
