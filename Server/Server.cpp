#ifndef MONGO_DATABASE
#include "SqliteDatabase.h"
#else
#include "MongoDatabase.h"
#endif
#include "Server.h"
#include <thread>
#include <iostream>

Server &Server::Instance() {
	static Server instance;
	return instance;
}

#ifndef MONGO_DATABASE 
Server::Server() : m_database( &SqliteDatabase::Instance() ) {}
#else
Server::Server() : m_database( &MongoDatabase::Instance() ) {}
#endif

Server::~Server() {}

void Server::run() {
	// create handle new client thread
	std::thread threadHandleNewClient( &Communicator::startHandleRequests, &Communicator::Instance() );
	threadHandleNewClient.detach();
	// exit from server on "EXIT"
	std::string line;
	do {
		std::getline( std::cin, line );
	} while ( line != "EXIT" );
}

IDatabase *Server::database() {
	return this->m_database;
}