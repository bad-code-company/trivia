#ifndef SERVER_H
#define SERVER_H

#include "Communicator.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Server {
private:
	/// fields
	IDatabase *m_database;

	/**
	 * @brief Constructor
	 */
	Server();
public:
	/**
	 * @brief Returns server instance
	 * @return Server instance
	 */
	static Server &Instance();

	/// Delete copy constructor
	Server( const Server & ) = delete;
	/// delete = operator
	void operator=( const Server & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~Server();

	/**
	 * @brief Starts a server
	 */
	void run();

	/**
	 * @brief Returns database
	 * @return Database
	 */
	IDatabase *database();
};

#endif // SERVER_H
