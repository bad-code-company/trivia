#ifndef MONGO_DATABASE

#include "SqliteDatabase.h"
#include "HttpsRequest.h"
#include <nlohmann/json.hpp>
#include <Base64.h>

#define DATABASE_FILENAME	"trivia.db"
#define QUESTIONS_QUANTITY	10

SqliteDatabase &SqliteDatabase::Instance() {
	static SqliteDatabase instance;
	return instance;
}

SqliteDatabase::SqliteDatabase() : IDatabase() {
	// open database
	if ( sqlite3_open( DATABASE_FILENAME, &this->m_database ) != SQLITE_OK ) {
		this->m_database = nullptr;
		throw std::exception( "Can't open database" );
	}
	char *errorMessage = nullptr;
	// enable foreign
	sqlite3_exec( this->m_database, "PRAGMA foreign_keys = ON;", nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
	// create users table
	sqlite3_exec( this->m_database, "CREATE TABLE IF NOT EXISTS USERS( "
									"USERNAME 		TEXT PRIMARY KEY NOT NULL, "
									"PASSWORD		TEXT NOT NULL, "
									"EMAIL 			TEXT NOT NULL "
									");", nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
	// create games table
	sqlite3_exec( this->m_database, "CREATE TABLE IF NOT EXISTS GAMES( "
									"ID				INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL "
									");", nullptr, nullptr, &errorMessage );
	// create questions table
	sqlite3_exec( this->m_database, "CREATE TABLE IF NOT EXISTS QUESTIONS( "
									"ID 			INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
									"QUESTION 		TEXT NOT NULL, "
									"CORRECT_ANSWER	TEXT NOT NULL, "
									"ANSWER2		TEXT NOT NULL, "
									"ANSWER3		TEXT NOT NULL, "
									"ANSWER4		TEXT NOT NULL "
									");", nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
	// create statistics table
	sqlite3_exec( this->m_database, "CREATE TABLE IF NOT EXISTS STATISTICS( "
									"USERNAME			TEXT REFERENCES USERS( USERNAME ) NOT NULL, "
									"GAMES_PLAYED		INTEGER NOT NULL, "
									"TOTAL_ANSWERS		INTEGER NOT NULL, "
									"CORRECT_ANSWERS	INTEGER NOT NULL, "
									"ANSWER_TIME		REAL NOT NULL "
									");", nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
	this->addQuestionsToDatabase( QUESTIONS_QUANTITY );
}

SqliteDatabase::~SqliteDatabase() {
	// close database
	if ( this->m_database ) {
		sqlite3_close( this->m_database );
		this->m_database = nullptr;
	}
}

std::string SqliteDatabase::fixQuotesForSQLite( const std::string &originalString ) {
	std::string fixedString;
	for ( auto c : originalString ) {
		if ( c == '\"' ) fixedString.append( "\"\"" );
		else fixedString.push_back( c );
	}
	return fixedString;
}

void SqliteDatabase::addQuestionsToDatabase( uint32_t quantity ) {
	// perform https request
	HttpsRequest httpsRequest( "opentdb.com" );
	std::string body = httpsRequest.get( "/api.php?amount=" + std::to_string( quantity ) + "&type=multiple&encode=base64" );
	if ( !body.empty() ) {
		// parse json
		nlohmann::json json = nlohmann::json::parse( body );
		// check response code
		if ( json.contains( "response_code" ) && json["response_code"].get<uint32_t>() == 0 ) {
			for ( uint32_t i = 0; i < quantity; ++i ) {
				// decode base64
				std::string question, correctAnswer, secondAnswer, thirdAnswer, fourthAnswer;
				macaron::Base64::Decode( json["results"][i]["question"].get<std::string>(), question );
				macaron::Base64::Decode( json["results"][i]["correct_answer"].get<std::string>(), correctAnswer );
				macaron::Base64::Decode( json["results"][i]["incorrect_answers"][0].get<std::string>(), secondAnswer );
				macaron::Base64::Decode( json["results"][i]["incorrect_answers"][1].get<std::string>(), thirdAnswer );
				macaron::Base64::Decode( json["results"][i]["incorrect_answers"][2].get<std::string>(), fourthAnswer );
				// insert
				std::string sqlInsertQuestion = "INSERT INTO QUESTIONS( QUESTION, CORRECT_ANSWER, ANSWER2, ANSWER3, ANSWER4 ) "
					"VALUES( \"" + SqliteDatabase::fixQuotesForSQLite( question ) + "\", \"" + SqliteDatabase::fixQuotesForSQLite( correctAnswer ) + "\", \"" + SqliteDatabase::fixQuotesForSQLite( secondAnswer ) + "\", \"" + SqliteDatabase::fixQuotesForSQLite( thirdAnswer ) + "\", \"" + SqliteDatabase::fixQuotesForSQLite( fourthAnswer ) + "\" );";
				char *errorMessage = nullptr;
				sqlite3_exec( this->m_database, sqlInsertQuestion.c_str(), nullptr, nullptr, &errorMessage );
				sqlite3_free( errorMessage );
			}
		}
	}
}

int SqliteDatabase::callbackIntToData( void *data, int, char **argv, char ** ) {
	*static_cast<int *>( data ) = std::atoi( *argv );
	return 0;
}

bool SqliteDatabase::doesUserExist( const std::string &username ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// get users with this username number
	std::string sqlCountUsersWithUsername = "SELECT COUNT( * ) FROM USERS WHERE USERNAME = \"" + username + "\";";
	int usersCount = 0;
	sqlite3_exec( this->m_database, sqlCountUsersWithUsername.c_str(), callbackIntToData, &usersCount, &errorMessage );
	sqlite3_free( errorMessage );
	return usersCount;
}

bool SqliteDatabase::doesPasswordMatch( const std::string &username, const std::string &password ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// get users with this username number and password
	std::string sqlCountUsersWithUsername = "SELECT COUNT( * ) FROM USERS WHERE USERNAME = \"" + username + "\" AND PASSWORD = \"" + password + "\";";
	int usersCount = 0;
	sqlite3_exec( this->m_database, sqlCountUsersWithUsername.c_str(), callbackIntToData, &usersCount, &errorMessage );
	sqlite3_free( errorMessage );
	return usersCount;
}

void SqliteDatabase::addNewUser( const std::string &username, const std::string &password, const std::string &email ) {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// insert user
	std::string sqlAddUser = "INSERT INTO USERS( USERNAME, PASSWORD, EMAIL ) " // sql insert user
		"VALUES( \"" + username + "\", \"" + password + "\", \"" + email + "\" )";
	sqlite3_exec( this->m_database, sqlAddUser.c_str(), nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
}

int SqliteDatabase::callbackQuestionVectorToData( void *data, int, char **argv, char ** ) {
	static_cast<std::vector<Question> *>( data )->push_back( Question( argv[1], argv[2], { argv[2], argv[3], argv[4], argv[5] } ) );
	return 0;
}

std::vector<Question> SqliteDatabase::getQuestions( const int quantity ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// create questions list
	std::vector<Question> questions;
	// get all data
	std::string sqlGetQuestions = "SELECT * FROM QUESTIONS ORDER BY RANDOM() LIMIT " + std::to_string( quantity );
	sqlite3_exec( this->m_database, sqlGetQuestions.c_str(), callbackQuestionVectorToData, &questions, &errorMessage );
	sqlite3_free( errorMessage );
	return questions;
}

int SqliteDatabase::callbackStringToData( void *data, int, char **argv, char ** ) {
	*static_cast<std::string *>( data ) = std::string( *argv );
	return 0;
}

float SqliteDatabase::getPlayerAverageAnswerTime( const std::string &username ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// get users with this username number and password
	std::string sqlAvgAnswerTime = "SELECT ANSWER_TIME FROM STATISTICS WHERE USERNAME = \"" + username + "\";";
	std::string avgAnswerTime;
	sqlite3_exec( this->m_database, sqlAvgAnswerTime.c_str(), callbackStringToData, &avgAnswerTime, &errorMessage );
	sqlite3_free( errorMessage );
	if ( avgAnswerTime.empty() ) throw std::exception( "No statistics for current user" );
	return std::stof( avgAnswerTime );
}

int SqliteDatabase::getNumOfCorrectAnswers( const std::string &username ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// count is correct where user
	std::string sqlCountCorrectAnswers = "SELECT CORRECT_ANSWERS FROM STATISTICS WHERE USERNAME = \"" + username + "\";";
	std::string correctAnswersCount;
	sqlite3_exec( this->m_database, sqlCountCorrectAnswers.c_str(), callbackStringToData, &correctAnswersCount, &errorMessage );
	sqlite3_free( errorMessage );
	if ( correctAnswersCount.empty() ) throw std::exception( "No statistics for current user" );
	return std::stoi( correctAnswersCount );
}

int SqliteDatabase::getNumOfTotalAnswers( const std::string &username ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// count is correct where user
	std::string sqlCountAnswers = "SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = \"" + username + "\";";
	std::string answersCount;
	sqlite3_exec( this->m_database, sqlCountAnswers.c_str(), callbackStringToData, &answersCount, &errorMessage );
	sqlite3_free( errorMessage );
	if ( answersCount.empty() ) throw std::exception( "No statistics for current user" );
	return std::stoi( answersCount );
}

int SqliteDatabase::getNumOfPlayerGames( const std::string &username ) const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// count is correct where user
	std::string sqlCountGames = "SELECT GAMES_PLAYED FROM STATISTICS WHERE USERNAME = \"" + username + "\";";
	std::string gamesCount;
	sqlite3_exec( this->m_database, sqlCountGames.c_str(), callbackStringToData, &gamesCount, &errorMessage );
	sqlite3_free( errorMessage );
	if ( gamesCount.empty() ) throw std::exception( "No statistics for current user" );
	return std::stoi( gamesCount );
}

int SqliteDatabase::callbackPlayersVectorToData( void *data, int, char **argv, char ** ) {
	static_cast<std::vector<std::string> *>( data )->push_back( std::string( argv[0] ) + ": " + std::string( argv[1] ) );
	return 0;
}

std::vector<std::string> SqliteDatabase::getBestPlayers() const {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// best players vector
	std::vector<std::string> bestPlayers;
	// select best players
	std::string sqlCountGames = "SELECT USERNAME, CORRECT_ANSWERS FROM STATISTICS ORDER BY CORRECT_ANSWERS DESC LIMIT 5";
	sqlite3_exec( this->m_database, sqlCountGames.c_str(), callbackPlayersVectorToData, &bestPlayers, &errorMessage );
	sqlite3_free( errorMessage );
	return bestPlayers;
}

void SqliteDatabase::addNewGame() {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// insert new game
	std::string sqlInsertToGame = "INSERT INTO GAMES VALUES( NULL )";
	sqlite3_exec( this->m_database, sqlInsertToGame.c_str(), nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
}

void SqliteDatabase::updateUserStatistics( const std::string &username, const int gamesPlayed, const int totalAnswers, const int correctAnswers, const float answerTime ) {
	if ( !this->m_database ) throw std::exception( "Database is not opened" );
	char *errorMessage = nullptr;
	// remove old statistics
	std::string sqlRemoveStatistics = "DELETE FROM STATISTICS WHERE USERNAME = " + username + ";";
	sqlite3_exec( this->m_database, sqlRemoveStatistics.c_str(), nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
	// insert new statistics
	std::string sqlInsertStatistics = "INSERT INTO STATISTICS VALUES( \"" + username + "\", " + std::to_string( gamesPlayed ) + ", " + std::to_string( totalAnswers ) + ", " + std::to_string( correctAnswers ) + ", " + std::to_string( answerTime ) + " );";
	sqlite3_exec( this->m_database, sqlInsertStatistics.c_str(), nullptr, nullptr, &errorMessage );
	sqlite3_free( errorMessage );
}

#endif // MONGO_DATABASE