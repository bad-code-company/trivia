#ifndef MONGO_DATABASE

#ifndef SQLITE_DATABASE_H
#define SQLITE_DATABASE_H

#include "IDatabase.h"
#include <sqlite3.h>

class SqliteDatabase : public IDatabase {
private:
	/// fields
	sqlite3 *m_database;

	/**
	 * @brief Construcotr
	 */
	SqliteDatabase();
public:
	/**
	 * @brief Returns sqlite database instance
	 * @return Sqlite database instance
	 */
	static SqliteDatabase &Instance();
	/// Delete copy constructor
	SqliteDatabase( const SqliteDatabase & ) = delete;
	/// delete = operator
	void operator=( const SqliteDatabase & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~SqliteDatabase();

	/**
	 * @brief Checks does user exist
	 * @param username User name to find
	 * @return Does user exist
	 */
	virtual bool doesUserExist( const std::string &username ) const;
	/**
	 * @brief Checks does password match to user (according to user name)
	 * @param username User's name
	 * @param password Password to check
	 * @return Does password match to user
	 */
	virtual bool doesPasswordMatch( const std::string &username, const std::string &password ) const;
	/**
	 * @brief Adds new user to a database
	 * @param username User's name to add
	 * @param password User's password
	 * @param email User's email
	 */
	virtual void addNewUser( const std::string &username, const std::string &password, const std::string &email );

	/**
	 * @brief Returns 'quantity' questions from the database
	 * @param quantity Question quantity
	 * @return 'Quantity' questions from the database
	 */
	virtual std::vector<Question> getQuestions( const int quantity ) const;
	/**
	 * @brief Returns user's average answer time
	 * @param username User's username
	 * @return User's average answer time
	 */
	virtual float getPlayerAverageAnswerTime( const std::string &username ) const;
	/**
	 * @brief Returns number of correct answers of the user
	 * @param username User's username
	 * @return Number of correct answers of the user
	 */
	virtual int getNumOfCorrectAnswers( const std::string &username ) const;
	/**
	 * @brief Returns number of answers of the user
	 * @param username User's username
	 * @return Number of answers of the user
	 */
	virtual int getNumOfTotalAnswers( const std::string &username ) const;
	/**
	 * @brief Returns number of games of the user
	 * @param username User's username
	 * @return Number of games of the user
	 */
	virtual int getNumOfPlayerGames( const std::string &username ) const;
	/**
	 * @brief Returns vector of 5 best players
	 * @return Vector of 5 best players
	 */
	virtual std::vector<std::string> getBestPlayers() const;

	/**
	 * @brief Adds new game to database
	 */
	virtual void addNewGame();
	/**
	 * @brief Updates user statistics
	 * @param username User to update stats
	 * @param gamesPlayed New games played value
	 * @param totalAnswers New total answers value
	 * @param correctAnswers New correct answers value
	 * @param answerTime New answer time value
	 */
	virtual void updateUserStatistics( const std::string &username, const int gamesPlayed, const int totalAnswers, const int correctAnswers, const float answerTime );
private:
	/**
	 * @brief Adds new questions to db
	 * @param quantity Number of questions to add
	 */
	virtual void addQuestionsToDatabase( uint32_t quantity );
	/// help methods
	/**
	 * @brief Fixes quotes in string that it can be added to db (" -> "")
	 * @param originalString Original string to fix
	 * @return Fixed string
	 */
	static std::string fixQuotesForSQLite( const std::string &originalString );
	/// callbacks
	/**
	 * @brief Callback that puts argv value to data ( int )
	 * @param data Pointer to data ( int * )
	 * @param argc Count of arguments
	 * @param argv Arguments array
	 * @param colNames Column names array
	 * @return Callback code ( 0 if everything good else 1 )
	 */
	static int callbackIntToData( void *data, int argc, char **argv, char **colNames );

	/**
	 * @brief Callback that puts argv value to vector of questions
	 * @param data Pointer to list ( std::vector<Question> * )
	 * @param argc Count of arguments
	 * @param argv Arguments array
	 * @param colNames Column names array
	 * @return Callback code ( 0 if everything good else 1 )
	 */
	static int callbackQuestionVectorToData( void *data, int argc, char **argv, char **colNames );
	/**
	 * @brief Callback that puts argv value to data ( std::string )
	 * @param data Pointer to data ( std::string * )
	 * @param argc Count of arguments
	 * @param argv Arguments array
	 * @param colNames Column names array
	 * @return Callback code ( 0 if everything good else 1 )
	 */
	static int callbackStringToData( void *data, int argc, char **argv, char **colNames );
	/**
	 * @brief Callback that puts argv value to data ( std::vector<std::string> )
	 * @param data Pointer to data ( std::vector<std::string> * )
	 * @param argc Count of arguments
	 * @param argv Arguments array
	 * @param colNames Column names array
	 * @return Callback code ( 0 if everything good else 1 )
	 */
	static int callbackPlayersVectorToData( void *data, int argc, char **argv, char **colNames );
};

#endif // SQLITE_DATABASE_H

#endif // MONGO_DATABASE