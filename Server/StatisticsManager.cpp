#include "StatisticsManager.h"
#include "Server.h"

StatisticsManager &StatisticsManager::Instance() {
	static StatisticsManager instance;
	return instance;
}

StatisticsManager::StatisticsManager() {}

StatisticsManager::~StatisticsManager() {}

std::vector<std::string> StatisticsManager::getHighScore() const {
	if ( !Server::Instance().database() ) throw std::exception( "Database is nullptr" );
	return Server::Instance().database()->getBestPlayers();
}

std::vector<std::string> StatisticsManager::getUserStatistics( const std::string &username ) const {
	if ( !Server::Instance().database() ) throw std::exception( "Database is nullptr" );
	std::vector<std::string> userStatistics;
	try {
		userStatistics.push_back( "Games played: " + std::to_string( Server::Instance().database()->getNumOfPlayerGames( username ) ) );
		userStatistics.push_back( "Total answers: " + std::to_string( Server::Instance().database()->getNumOfTotalAnswers( username ) ) );
		userStatistics.push_back( "Correct answers: " + std::to_string( Server::Instance().database()->getNumOfCorrectAnswers( username ) ) );
		userStatistics.push_back( "Average answer time: " + std::to_string( Server::Instance().database()->getPlayerAverageAnswerTime( username ) ) );
	} catch ( ... ) {
		userStatistics.clear();
		userStatistics.push_back( "Games played: null" );
		userStatistics.push_back( "Total answers: null" );
		userStatistics.push_back( "Correct answers: null" );
		userStatistics.push_back( "Average answer time: null" );
	}
	return userStatistics;
}

void StatisticsManager::updateUserStatistics( const LoggedUser &user, const int totalAnswers, const int correctAnswers, const float answerTime ) {
	IDatabase *database = Server::Instance().database();
	if ( !database ) throw std::exception( "Database is nullptr" );
	// update stats to db
	try {
		database->updateUserStatistics(
			user.getUsername(),
			database->getNumOfPlayerGames( user.getUsername() ) + 1,
			database->getNumOfTotalAnswers( user.getUsername() ) + totalAnswers,
			database->getNumOfCorrectAnswers( user.getUsername() ) + correctAnswers,
			database->getPlayerAverageAnswerTime( user.getUsername() ) + answerTime / 2.f
		);
	} catch ( ... ) {
		database->updateUserStatistics(
			user.getUsername(),
			1,
			totalAnswers,
			correctAnswers,
			answerTime
		);
	}
}