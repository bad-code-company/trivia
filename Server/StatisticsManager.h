#ifndef STATISTICS_MANAGER_H
#define STATISTICS_MANAGER_H

#include <string>
#include <vector>
#include "IDatabase.h"
#include "LoggedUser.h"

class StatisticsManager {
private:
	/**
	 * @brief Contructor
	 */
	StatisticsManager();
public:
	/**
	 * @brief Returns statistics manager factory
	 * @return Statistics manager instance
	 */
	static StatisticsManager &Instance();
	/// Delete copy constructor
	StatisticsManager( const StatisticsManager & ) = delete;
	/// delete = operator
	void operator=( const StatisticsManager & ) = delete;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~StatisticsManager();

	/**
	 * @brief Returns top 5 best players
	 * @return Top 5 best players
	 */
	std::vector<std::string> getHighScore() const;
	/**
	 * @brief Returns user statistics (games played, total answers, correct answers, average answer time)
	 * @param username User's username
	 * @return User statistics (games played, total answers, correct answers, average answer time)
	 */
	std::vector<std::string> getUserStatistics( const std::string &username ) const;

	/**
	 * @brief Updates user's statistics
	 * @param user User to update stats
	 * @param totalAnswers Total answers to add to current stats
	 * @param correctAnswers Correct answers to add to current stats
	 * @param answerTime Answer time to add to current stats
	 */
	void updateUserStatistics( const LoggedUser &user, const int totalAnswers, const int correctAnswers, const float answerTime );
};
#endif // STATISTICS_MANAGER_H
