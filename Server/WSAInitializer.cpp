#include "WSAInitializer.h"
#include <WinSock2.h>
#include <Windows.h>
#include <exception>

WSAInitializer::WSAInitializer() {
	WSADATA wsaData;
	if ( WSAStartup( MAKEWORD( 2, 2 ), &wsaData ) ) throw std::exception( "WSAStartup Failed" );
}

WSAInitializer::~WSAInitializer() {
	try {
		WSACleanup();
	} catch ( ... ) {}
}
