#ifndef WSA_INITILIAZER_H
#define WSA_INITILIAZER_H

class WSAInitializer {
public:
	/**
	 * @brief WSA Initializer
	 */
	WSAInitializer();
	/**
	 * @brief WSA Deinitializer
	 */
	~WSAInitializer();
};

#endif // WSA_INITILIAZER_H