#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>

int main( int argc, char *argv[] ) {
	try {
		WSAInitializer wsaInitializer;
		Server::Instance().run();
	} catch ( const std::exception &e ) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}